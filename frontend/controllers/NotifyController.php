<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/24/2015
 * Time: 1:23 PM
 */
namespace frontend\controllers;

use frontend\models\NotificationQuery;
use yii\web\Controller;

class NotifyController extends Controller
{
    public function actionIndex()
    {
        $notification_query = NotificationQuery::getInstance();
        $to_user_id = \Yii::$app->user->getId();
        $notifications = $notification_query->get_all_notification($to_user_id);
        return $this->render('index', ['notifications' => $notifications]);
    }
}