<?php
namespace frontend\controllers;

use app\models\MessageGroup;
use DateTime;
use frontend\models\FriendQuery;
use frontend\models\MessageForm;
use frontend\models\MessageQuery;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MessageController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionCreate()
    {
        $model = new MessageForm();
        $friend_query = FriendQuery::getInstance();
        $friends = $this->toSelectData($friend_query->get_friends(\Yii::$app->user->getId()));
        if ($model->load(\Yii::$app->request->post())) {
            $model->send_user_id = \Yii::$app->user->getId();
            $model->sent_at = (new DateTime())->format('y-m-d h:i:s');
            $model->create();
            return \Yii::$app->response->redirect(Url::to(['message/detail', 'group_id' => $model->getMessageGroupID()]));
        }
        return $this->render('index', ['is_create' => true, 'model' => $model, 'friends' => $friends]);
    }

    public function actionIndex()
    {
        return \Yii::$app->response->redirect(Url::to(['message/inbox']));
    }

    public function actionSent()
    {
        $message_query = MessageQuery::getInstance();
        $message_sents = $message_query->get_all_message_sent(\Yii::$app->user->getId());
        return $this->render('index', ['is_sent' => true, 'messages' => $message_sents]);
    }

    public function actionInbox()
    {
        $received_user_id = \Yii::$app->user->getId();
        $message_query = MessageQuery::getInstance();
        $messages = $message_query->get_all_message_inbox($received_user_id);

        return $this->render('index', ['is_inbox' => true, 'messages' => $messages]);
    }

    public function actionDetail($group_id)
    {
        $message_query = MessageQuery::getInstance();
        $is_viewable = $message_query->is_viewable($group_id, \Yii::$app->user->getId());
        if ($is_viewable) {
            $messages = $message_query->get_messages_in_group($group_id);
            $members = $message_query->get_user_in_group($group_id);
            $message['subject'] = MessageGroup::findOne(['id' => $group_id])['name'];
            $message['messages'] = $messages;
            $message['members'] = $members;
            $message['group_id'] = $group_id;
            if (isset($_POST['reply_message_content'])) {
                $message_content = $_POST['reply_message_content'];
                $send_user_id = \Yii::$app->user->getId();
                $message_query->reply($send_user_id, $group_id, $message_content);
                $messages = $message_query->get_messages_in_group($group_id);
                $message['messages'] = $messages;
                return $this->render('index', ['is_detail' => true, 'message' => $message]);
            }
            return $this->render('index', ['is_detail' => true, 'message' => $message]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionDeleteMessage()
    {
        if (isset($_POST['mess_id'])) {
            $message_id = $_POST['mess_id'];
            $message_query = MessageQuery::getInstance();
            $message_query->delete_message($message_id);
        }
    }

    public function actionLeave()
    {
        if (isset($_POST['group_id'])) {
            $group_id = $_POST['group_id'];
            $user_id = \Yii::$app->user->getId();
            $message_query = MessageQuery::getInstance();
            $message_query->leave_group_message($group_id, $user_id);
        }
    }

    private function toSelectData($friends)
    {
        $data = [];
        foreach ($friends as $friend) {
            $data[$friend['id']] = $friend['full_name'];
        }
        return $data;
    }
}