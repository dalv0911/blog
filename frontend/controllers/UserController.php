<?php
/**
 * Created by PhpStorm.
 * User: chungtv
 * Date: 10/19/2015
 * Time: 8:05 PM
 */

namespace frontend\controllers;

use app\models\Friend;
use app\models\Notification;
use frontend\models\FriendQuery;
use frontend\models\PostQuery;
use frontend\models\SettingForm;
use frontend\models\UserQuery;
use Yii;
use common\models\User;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->getId() !== null) {
            $model = $this->findModel(Yii::$app->user->getId());
            return $this->render('index', ['model' => $model]);
        } else {
            throw new HttpException(403, 'You are not permission for this page');
        }
    }


    public function actionProfile($id)
    {

        $userQuery = UserQuery::getInstance();
        $postQuery = PostQuery::getInstance();
        $posts = $postQuery->getRecentPosts($id);
        $friendQuery = FriendQuery::getInstance();
        $friendList = $friendQuery->get_all_friend($id, 2);
        $familyList = $friendQuery->get_all_friend($id, 1);
        $model = $userQuery->getUserById($id);
        $my_id = Yii::$app->user->getId();
        if ($id == Yii::$app->user->getId()) {
            $setting = new SettingForm();
            if ($setting->load(Yii::$app->request->post()) && $setting->validate()) {
                $isUpdated = false;
                if ($setting->updateInfo()) {
                    $isUpdated = true;
                }
                return $this->render('profile', ['model' => $model, 'setting' => $setting, 'isUpdated' => $isUpdated,
                    'posts' => $posts, 'friendList' => $friendList, 'familyList' => $familyList]);
            } else {
                return $this->render('profile', ['model' => $model, 'setting' => $setting, 'posts' => $posts,
                    'friendList' => $friendList, 'familyList' => $familyList]);
            }
        } else {
            if (isset($_POST['group_friend'])) {
                $group_id = $_POST['group_friend'];
                $send_request_user_id = $my_id;
                $received_request_user_id = $id;
                $userQuery->make_friend($send_request_user_id, $received_request_user_id, $group_id);
            }
            $relative_code = $userQuery->get_relative($my_id, $id);
            if (!empty($model)) {
                return $this->render('profile', ['model' => $model, 'relative_code' => $relative_code,
                    'posts' => $posts, 'friendList' => $friendList, 'familyList' => $familyList]);
            } else {
                throw new HttpException(404, 'The requested user could not be found.');
            }
        }
    }

    public function actionAcceptFriend()
    {
        if (isset($_POST['notify_id'])) {
            $friend_query = FriendQuery::getInstance();
            $notify_id = $_POST['notify_id'];
            if ($friend_query->accept_friend($notify_id)) {
                Notification::deleteAll(['id' => $notify_id]);
                echo "YES";
            } else {
                echo "NO";
            }
        } else {
            echo "NO";
        }
    }

    public function actionRefuseFriend()
    {
        if (isset($_POST['notify_id'])) {
            $notification = Notification::findOne(['id' => $_POST['notify_id']]);
            $send_request_user_id = $notification['from_user_id'];
            $received_request_user_id = $notification['to_user_id'];
            Friend::deleteAll(['send_request_user_id' => $send_request_user_id, 'received_request_user_id' => $received_request_user_id]);
            Notification::deleteAll(['id' => $_POST['notify_id']]);
            return "YES";
        } else {
            return "NO";
        }
    }

    private function findModel($id)
    {

        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("The request page do not exist!");
        }
    }

}