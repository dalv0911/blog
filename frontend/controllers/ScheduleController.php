<?php

/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 11/13/2015
 * Time: 12:17 PM
 */
namespace frontend\controllers;

use app\models\Schedule;
use frontend\models\FriendQuery;
use frontend\models\ScheduleForm;
use frontend\models\ScheduleQuery;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii2fullcalendar\models\Event;

class ScheduleController extends Controller
{
    public function actionIndex()
    {
        $model = new ScheduleForm();
        $schedule_query = ScheduleQuery::getInstance();
        if ($model->load(\Yii::$app->request->post())) {
            $model->owner_id = \Yii::$app->user->getId();
            $schedule_query->newSchedule($model);
        }
        $schedules = $schedule_query->getSchedules(\Yii::$app->user->getId());
        $events = $this->convert_to_events($schedules);
        return $this->render('index', ['model' => $model, 'events' => $events, 'friends' => $friends]);
    }

    public function actionEdit($id)
    {
        $model = new ScheduleForm();

        $schedule = Schedule::findOne(['id' => $id]);
        $this->init_schedule_form($model, $schedule);

        $schedule_query = ScheduleQuery::getInstance();
        $schedules = $schedule_query->getSchedules(\Yii::$app->user->getId());

        $events = $this->convert_to_events($schedules);
        if ($model->load(\Yii::$app->request->post())) {
            $model->owner_id = \Yii::$app->user->getId();
            $schedules = $schedule_query->getSchedules(\Yii::$app->user->getId());
            $events = $this->convert_to_events($schedules);
            $schedule_query->editSchedule($model, $id);
            return $this->render('index', ['model' => $model, 'events' => $events, 'edit' => true, 'edited' => true]);
        }
        return $this->render('index', ['model' => $model, 'events' => $events, 'edit' => true]);
    }

    public function actionDetail($id)
    {
        $schedule_query = ScheduleQuery::getInstance();
        $schedule = $schedule_query->getSchedule($id);
        if (empty($schedule)) {
            return new BadRequestHttpException();
        }
        return $this->render('detail', ['schedule' => $schedule]);
    }

    public function actionDelete()
    {
        if (isset($_POST['id'])) {
            $schedule_id = $_POST['id'];
            $schedule_query = ScheduleQuery::getInstance();
            if ($schedule_query->delete($schedule_id)) {
                return "YES";
            } else {
                return "NO";
            }
        }
    }

    private function convert_to_events($schedules)
    {

        $events[] = array();
        $index = 0;
        foreach ($schedules as $schedule) {
            $event = new Event();
            $event->id = $schedule['id'];
            $event->title = $schedule['subject'];
            $event->start = date('Y-m-d\TH:i:s\Z', strtotime($schedule['from']));
            $event->end = date('Y-m-d\TH:i:s\Z', strtotime($schedule['to']));
            $event->url = '?r=schedule/detail&id=' . $schedule['id'];
            $event->backgroundColor = $schedule['color'];
            $events[$index++] = $event;
        }
        return $events;
    }

    private function init_schedule_form($model, $schedule)
    {
        $model['subject'] = $schedule['subject'];
        $model['content'] = $schedule['content'];
        $model['address'] = $schedule['address'];
        $model['from'] = $schedule['from'];
        $model['to'] = $schedule['to'];
        $model['color'] = $schedule['color'];
    }

    private function toSelectData($friends)
    {
        $data = [];
        foreach ($friends as $friend) {
            $data[$friend['id']] = $friend['full_name'];
        }
        return $data;
    }

    private function toMapString($assigned_users)
    {
        $results = [];
        foreach ($assigned_users as $user) {
            $results[$user['id']] = $user['full_name'];
        }
        return $results;
    }
}