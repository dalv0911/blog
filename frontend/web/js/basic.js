$(document).ready(function () {
    $('.del_comment').click(function () {
        var id = $(this).attr('id');
        var data = "id=" + id;
        var container = $(this).parent().parent().parent();
        $.ajax({
            type: "POST",
            url: "?r=comment/delete",
            data: data,
            success: function (respone) {
                if (respone == "NO") {
                    alert("Comment chưa được xóa, vui lòng thử lại!");
                } else {
                    container.slideUp('slow', function () {
                        container.remove();
                    });
                }
            }
        });
    });
    $('.add_friend').click(function () {
        $('#choice_group').show(1500);
        $('.add_friend').hide(1000);
    });
    $('.accept_friend').click(function () {
        var id = $(this).attr('id');
        var data = "notify_id=" + id;
        var container = $(this).parent().parent();
        var parent = container.parent();
        $.ajax({
            type: "POST",
            url: "?r=user/accept-friend",
            data: data,
            success: function (response) {
                if (response == "YES") {
                    alert("Bạn đã có thêm 1 người mới trong danh sách bạn bè.");
                    parent.slideUp('slow', function () {
                        parent.remove();
                    });
                } else {
                    alert("Có lỗi xảy ra, vui lòng thử lại lần nữa.");
                }
            }
        });
    });
    $('.refuse_friend').click(function () {
        var id = $(this).attr('id');
        var data = "notify_id=" + id;
        var container = $(this).parent().parent();
        var parent = container.parent();
        $.ajax({
            type: 'POST',
            url: '?r=user/refuse-friend',
            data: data,
            success: function (response) {
                if (response == "YES") {
                    parent.slideUp('slow', function () {
                        parent.remove();
                    });
                }
            }
        });
    });
    $('.content_cmt').keydown(function (e) {
        if (e.keyCode == 13) {
            var content = $(this).val();
            var data = $(this).attr("id");
            var params = data + "&content=" + content;
            var container_cmt = $('#box-comment');
            $.ajax({
                type: "POST",
                url: "?r=comment/create",
                data: params,
                success: function (response) {
                    if (response != "NO") {
                        container_cmt.append(response);
                        $(this).val = "";
                    }
                }
            });
        }
    });
    $('.select_friend').click(function () {
        alert("Clicked !!!");
    });
    $('.show_message_form').click(function () {
        var reply_message_form = $('.reply_message_form');
        if (!reply_message_form.active) {
            reply_message_form.show(10);
            $('.show_message_form').hide(10);
            $('.reply_message').show(10);
        }
    });
    $('.delete_message_item').click(function () {
        var message_id = $(this).attr('id');
        var message_item = $('.message_item' + message_id);
        var question = "Bạn có chắc chắn muốn xóa tin nhắn này?";
        var data = 'mess_id=' + message_id;
        if (confirm(question)) {
            $.ajax({
                type: 'POST',
                url: '?r=message/delete-message',
                data: data,
                success: function () {
                    message_item.hide(1000);
                }
            });
        }
    });
    $('.leave_group_message').click(function () {
        var group_id = $(this).attr('id');
        var data = 'group_id=' + group_id;
        var question = "Bạn có chắc chắn muốn dời khỏi cuộc trò chuyện này?";
        if (confirm(question)) {
            $.ajax({
                type: 'POST',
                url: '?r=message/leave',
                data: data,
                success: function () {
                    window.location.replace("?r=site/index");
                }
            });
        }
    });
    $('.delete-post').click(function () {
        var id = $(this).attr('id');
        var data = "id=" + id;
        var question = "Bạn có chắc chắn muốn xóa bài viết này không?";
        if (confirm(question)) {
            $.ajax({
                type: 'POST',
                url: '?r=post/delete',
                data: data,
                success: function () {
                    alert("Xóa bài viết thành công !");
                    window.location.replace('?r=post');
                }
            });
        }
    });
    $('.delete-schedule').click(function () {
        var id = $(this).attr('id');
        var data = "id=" + id;
        var question = "Bạn có chắc chắn muốn xóa lịch này không?";
        if (confirm(question)) {
            $.ajax({
                type: 'POST',
                url: '?r=schedule/delete',
                data: data,
                success: function (response) {
                    if (response == "YES") {
                        window.location.replace('?r=schedule');
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
    });
});