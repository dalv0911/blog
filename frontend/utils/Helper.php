<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/21/2015
 * Time: 11:33 AM
 */
namespace frontend\utils;

use DateTime;
use yii\web\User;

class Helper
{
    static $SEND_REQUEST_MAKE_FRIEND = 1;
    static $RECEIVED_REQUEST_MAKE_FRIEND = 2;

    public static function print_privacy($privacy_id)
    {
        switch ($privacy_id) {
            case 1:
                return 'Chỉ mình tôi';
            case 2:
                return 'Chỉ một số bạn bè được quyền xem';
            case 3:
                return 'Tất cả bạn bè';
            case 4:
                return 'Công khai';
            default:
                return null;
        }
    }

    public static function permission_request()
    {
        if (\Yii::$app->user->isGuest)
            return false;
        return true;
    }

    public static function is_admin()
    {
        $user = \common\models\User::findOne(['id' => \Yii::$app->user->getId()]);
        $level = $user['level_id'];
        if ($level == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function calculate_time($time)
    {
        $time = new DateTime($time);
        $current = new DateTime();
        $interval = $time->diff($current);
        $hour = $interval->format('%h');
        $minutes = $interval->format('%i');
        $day = $interval->format('%d');
        $datetime = '';
        if ($day > 0) {
            $datetime = $day . " day ago";
        } else if ($hour > 0) {
            $datetime = $hour . " hour ago";
        } else if ($minutes > 0) {
            $datetime = $minutes . " minutes ago";
        }else{
            $datetime = "Just now";
        }
        return $datetime;
    }

}