<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:09 PM
 */
namespace frontend\models;

use app\models\Friend;
use app\models\Notification;
use frontend\utils\Helper;
use Yii;
use yii\db\Query;

class UserQuery
{

    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new UserQuery();
        } else {
            return static::$query;
        }
    }

    public function getUserById($id)
    {
        $model = (new Query())
            ->select(['user.id as id', 'CONCAT(first_name," ",last_name) as full_name', 'username', 'email', 'work', 'phone_number', 'user.address',
                'hobby', 'user.desc', 'user.image as image', 'level_id', 'user.created_at', 'COUNT(p.id) AS prod_num'])
            ->from('user')
            ->join('LEFT OUTER JOIN', 'post as p', 'user.id = p.owner_id')
            ->where(['user.id' => $id])->one();
        return $model;
    }

    public function searchUserByKeyword($keyword, $from_record)
    {
        $model = Yii::$app->db->createCommand('SELECT DISTINCT id,username,address,CONCAT(first_name," ",last_name) as full_name, ' .
            'image FROM user WHERE username LIKE :keyword OR first_name LIKE :keyword LIMIT :from,5');
        $model->bindValue(':keyword', '%' . $keyword . '%');
        $model->bindValue(':keyword', '%' . $keyword . '%');
        $model->bindValue(':from', $from_record);
        return $model->queryAll();
    }

    public function is_friend($user_id1, $user_id2)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT id' .
            ' FROM friend' .
            ' WHERE send_request_user_id=:user_id1 AND received_request_user_id=:user_id2 AND is_accepted=true'
        );
        $model->bindValues([':user_id1' => $user_id1, ':user_id2' => $user_id2]);
        $results = $model->queryAll();
        if (empty($results))
            return false;
        else
            return true;
    }

    public function is_send_request($my_id, $user_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT id ' .
            ' FROM friend' .
            ' WHERE send_request_user_id =:my_id AND received_request_user_id =:user_id AND is_accepted = false'
        );
        $model->bindValues([':my_id' => $my_id, ':user_id' => $user_id]);
        $results = $model->queryAll();
        if (empty($results))
            return false;
        else
            return true;
    }

    public function is_has_request($my_id, $user_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT id ' .
            ' FROM friend' .
            ' WHERE send_request_user_id =:user_id AND received_request_user_id =:my_id AND is_accepted = false'
        );
        $model->bindValues([':my_id' => $my_id, ':user_id' => $user_id]);
        $results = $model->queryAll();
        if (empty($results))
            return false;
        else
            return true;
    }

    /**
     * 1.No relative
     * 2.Cho accept
     * 3.has request
     * 4.Friend
     * @param $user_id1
     * @param $user_id2
     * @return int
     */
    public function get_relative($user_id1, $user_id2)
    {
        if ($this->is_send_request($user_id1, $user_id2)) {
            return 2;
        } elseif ($this->is_has_request($user_id1, $user_id2)) {
            return 3;
        } elseif ($this->is_friend($user_id1, $user_id2)) {
            return 4;
        } else {
            return 1;
        }
    }

    public function make_friend($send_request_user_id, $received_request_user_id, $group_id)
    {
        $model = new Friend();
        $notification_query = NotificationQuery::getInstance();
        $model->send_request_user_id = $send_request_user_id;
        $model->received_request_user_id = $received_request_user_id;
        $model->group_id = $group_id;
        $model->is_accepted = false;
        $notification_query->send_notification($send_request_user_id, $received_request_user_id, Helper::$SEND_REQUEST_MAKE_FRIEND);
        return $model->save();
    }
}