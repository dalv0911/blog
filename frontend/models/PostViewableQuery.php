<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 11/12/2015
 * Time: 12:18 PM
 */
namespace frontend\models;

use app\models\PostViewable;

class PostViewableQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new PostViewableQuery();
        } else {
            return static::$query;
        }
    }

    public function delete_all_by_post_id($post_id)
    {
        PostViewable::deleteAll(['post_id' => $post_id]);
    }

    public function add_all($post_id, $friend_ids)
    {
        foreach ($friend_ids as $user_id) {
            $post_viewable = new PostViewable();
            $post_viewable['post_id'] = $post_id;
            $post_viewable['user_id'] = $user_id;
            $post_viewable->save();
        }
    }

    public function get_viewable_friends($post_id)
    {
        $query = \Yii::$app->db->createCommand(
            ' SELECT user_id ' .
            ' FROM post_viewable ' .
            ' WHERE post_id = :post_id'
        );
        $query->bindValues([':post_id' => $post_id]);
        return $query->queryAll();
    }
}