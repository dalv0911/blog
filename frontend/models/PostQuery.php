<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:07 PM
 */
namespace frontend\models;

use app\models\Comment;
use app\models\Friend;
use app\models\GroupFriend;
use app\models\Post;
use app\models\PostTag;
use app\models\PostViewable;
use frontend\utils\Helper;
use Yii;

class PostQuery
{

    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new PostQuery();
        } else {
            return static::$query;
        }
    }

    public function getPostById($post_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,content,p.image as image,privacy_id,time,commentable,p.created_at,user.image as avatar,CONCAT(first_name," ",last_name) as full_name,owner_id ' .
            ' FROM post as p LEFT JOIN user ON owner_id = user.id' .
            ' WHERE p.id = :id'
        );
        $model->bindValues([':id' => $post_id]);
        return $model->queryOne();
    }

    public function getBeforePostForId($owner_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,p.image as image,time,p.created_at,content,privacy_id,user.image as avatar,CONCAT(first_name," ",last_name) as full_name,owner_id ' .
            ' FROM post as p LEFT JOIN user ON owner_id = user.id ' .
            ' WHERE owner_id = :owner_id ' .
            ' GROUP BY time DESC' .
            ' LIMIT 5'
        );
        $model->bindValues(['owner_id' => $owner_id]);
        return $model->queryAll();
    }

    public function getPostByOwnerId($owner_id, $page)
    {
        $from_record = $page * 10;
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id,title,content,p.image as image,time,privacy_id,p.created_at ' .
            ' FROM post as p ' .
            ' WHERE owner_id =:owner_id LIMIT :from_record,10'
        );
        $model->bindValues([':owner_id' => $owner_id, ':from_record' => $from_record]);
        return $model->queryAll();
    }

    public function readable($user_id, $owner_id, $post_id, $privacy)
    {
        if ($user_id == null)
            return false;
        else if ($user_id == $owner_id || Helper::is_admin())
            return true;
        switch ($privacy) {
            case 1:
                return false;
            case 2:
                $model = PostViewable::findOne(['user_id' => $user_id, 'post_id' => $post_id]);
                if ($model == null) {
                    return false;
                } else {
                    return true;
                }
            case 3:
                $model = Friend::findOne(['send_request_user_id' => $user_id, 'received_request_user_id' => $owner_id, 'is_accepted' => true]);
                if ($model == null) {
                    return false;
                } else {
                    return true;
                }
            case 4:
                return true;
            default:
                return true;
        }
    }

    public function getRecentPosts($user_id)
    {
        $query = Yii::$app->db->createCommand(
            ' SELECT *' .
            ' FROM post' .
            ' WHERE owner_id = :user_id' .
            ' GROUP  BY time DESC' .
            ' LIMIT 10'
        );
        $query->bindValue(':user_id', $user_id);
        return $query->queryAll();
    }

    public function getIndexPosts($user_id)
    {

        $query = Yii::$app->db->createCommand(
            ' SELECT DISTINCT post.owner_id,CONCAT(first_name," ",last_name) as full_name,user.image as avatar,post.id,title,privacy_id,post.created_at,content,time ' .
            ' FROM user LEFT JOIN post ON user.id = owner_id' .
            ' WHERE post.privacy_id = 4' .
            ' OR (post.privacy_id = 3 AND post.owner_id IN (SELECT friend.send_request_user_id
                                                          FROM friend
                                                          WHERE friend.received_request_user_id = :user_id
                                                          AND friend.is_accepted = 1))
            OR (post.privacy_id = 2 AND post.id IN (SELECT post_viewable.post_id
                                                   FROM post_viewable
                                       WHERE post_viewable.user_id = :user_id))
            OR owner_id = :user_id '.
            ' GROUP BY time ASC '.
            ' LIMIT 10'
        );
        $query->bindValues([':user_id' => $user_id]);
        return $query->queryAll();
    }

    public function deletePost($post_id)
    {
        PostTag::deleteAll(['post_id' => $post_id]);
        Comment::deleteAll(['post_id' => $post_id]);
        PostViewable::deleteAll(['post_id' => $post_id]);
        Post::deleteAll(['id' => $post_id]);
    }
}