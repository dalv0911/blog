<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 11/12/2015
 * Time: 12:17 PM
 */
namespace app\models;

use yii\db\ActiveRecord;

class PostViewable extends ActiveRecord
{
    public static function tableName()
    {
        return "post_viewable";
    }
}