<?php

namespace frontend\models;

use app\models\GroupUser;
use app\models\Message;
use DateTime;

class MessageQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new MessageQuery();
        } else {
            return static::$query;
        }
    }

    public function get_all_message_inbox($received_user_id)
    {
        $query = \Yii::$app->db->createCommand(
            'SELECT DISTINCT send_user_id,CONCAT(first_name," ",last_name) as full_name,message_group_id,mg.name as message_group_name,
                  message.sent_at ' .
            ' FROM user LEFT JOIN message ON user.id = message.send_user_id LEFT JOIN message_group as mg ON mg.id = received_group_id
                    LEFT JOIN group_user ON mg.id = message_group_id ' .
            ' WHERE group_user.user_id = :user_id AND send_user_id != :user_id' .
            ' GROUP BY received_group_id'
        );
        $query->bindValues([':user_id' => $received_user_id]);
        return $query->queryAll();
    }

    public function get_all_message_sent($send_user_id)
    {

        $query = \Yii::$app->db->createCommand(
            ' SELECT DISTINCT mg.id as message_group_id,mg.name as message_group_name,message.sent_at ' .
            ' FROM message_group as mg LEFT JOIN message ON mg.id = received_group_id ' .
            ' WHERE send_user_id = :send_user_id ' .
            ' GROUP BY received_group_id'
        );
        $query->bindValues([':send_user_id' => $send_user_id]);
        return $query->queryAll();
    }

    public function count_message_inbox($received_user_id)
    {
        $query = \Yii::$app->db->createCommand(
            ' SELECT COUNT(message.id) as count' .
            ' FROM message LEFT JOIN group_user ON message_group_id = received_group_id ' .
            ' WHERE group_user.user_id = :user_id'
        );
        $query->bindValues([':user_id' => $received_user_id]);
        return $query->queryOne()['count'];
    }

    public function get_messages_in_group($group_id)
    {
        $query = \Yii::$app->db->createCommand(
            ' SELECT mess.send_user_id,user.image,CONCAT(first_name," ",last_name) as full_name,mess.id as mess_id,mess.content,mess.sent_at ' .
            ' FROM user LEFT JOIN message as mess ON send_user_id = user.id LEFT JOIN group_user as gu ON received_group_id = gu.id ' .
            ' WHERE received_group_id = :group_id '
        );
        $query->bindValues([':group_id' => $group_id]);
        return $query->queryAll();
    }

    public function get_user_in_group($group_id)
    {

        $query = \Yii::$app->db->createCommand(
            ' SELECT user.id,user.image,CONCAT(first_name," ",last_name) as full_name' .
            ' FROM user LEFT JOIN group_user as gu ON user_id = user.id ' .
            ' WHERE message_group_id = :group_id '
        );
        $query->bindValues([':group_id' => $group_id]);
        return $query->queryAll();
    }

    public function reply($send_user_id, $received_group_id, $content)
    {
        $message = new Message();
        $message['send_user_id'] = $send_user_id;
        $message['received_group_id'] = $received_group_id;
        $message['content'] = $content;
        $message['sent_at'] = (new DateTime())->format('y-m-d h:i:s');
        $message->save();
    }

    public function delete_message($message_id)
    {
        $message = Message::findOne(['id' => $message_id])->delete();
    }

    public function leave_group_message($group_id, $user_id)
    {
        $group_user = GroupUser::findOne(['message_group_id' => $group_id, 'user_id' => $user_id])->delete();
    }

    public function is_viewable($group_id, $user_id)
    {
        $count = GroupUser::findAll(['message_group_id' => $group_id, 'user_id' => $user_id]);
        if (count($count) > 0) {
            return true;
        } else {
            return false;
        }
    }

}