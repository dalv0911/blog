<?php

namespace frontend\models;

use app\models\Schedule;
use app\models\ScheduleUser;
use DateTime;


class ScheduleQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new ScheduleQuery();
        } else {
            return static::$query;
        }
    }

    public function newSchedule($schedule_form)
    {
        $schedule = new Schedule();
        $schedule['subject'] = $schedule_form['subject'];
        $schedule['content'] = $schedule_form['content'];
        $schedule['from'] = $schedule_form['from'];
        $schedule['to'] = $schedule_form['to'];
        $schedule['owner_id'] = $schedule_form['owner_id'];
        $schedule['color'] = $schedule_form['color'];
        $schedule['address'] = $schedule_form['address'];
        $schedule['created_at'] = (new DateTime())->format('y-m-d h:i:s');
        $schedule->save();
    }

    public function editSchedule($schedule_form, $id)
    {
        $schedule = Schedule::findOne(['id' => $id]);
        $schedule['subject'] = $schedule_form['subject'];
        $schedule['content'] = $schedule_form['content'];
        $schedule['from'] = $schedule_form['from'];
        $schedule['to'] = $schedule_form['to'];
        $schedule['owner_id'] = $schedule_form['owner_id'];
        $schedule['color'] = $schedule_form['color'];
        $schedule['address'] = $schedule_form['address'];
        $schedule->save();
    }

    public function getSchedules($user_id)
    {
        $query = \Yii::$app->db->createCommand(
            ' SELECT DISTINCT schedule.id,subject,content,created_at,address,`from`,`to`,color,address ' .
            ' FROM schedule ' .
            ' WHERE (owner_id = :user_id) '
        );
        $query->bindValues([':user_id' => $user_id]);
        return $query->queryAll();
    }

    public function delete($id)
    {
        return Schedule::findOne(['id' => $id])->delete();
    }

    public function getSchedule($id)
    {
        $query = \Yii::$app->db->createCommand(
            ' SELECT schedule.id,subject,content,schedule.address,`from`,`to`,color,schedule.created_at,owner_id,CONCAT(first_name," ",last_name) as full_name,image ' .
            ' FROM user LEFT JOIN schedule ON owner_id = user.id ' .
            ' WHERE schedule.id = :id'
        );
        $query->bindValues([':id' => $id]);
        $schedule = $query->queryOne();
        return $schedule;
    }

}