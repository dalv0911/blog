<?php
namespace app\models;

use yii\db\ActiveRecord;

class GroupUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'group_user';
    }
}