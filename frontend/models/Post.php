<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 5:37 AM
 */
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Post extends ActiveRecord
{
    public static function tableName()
    {
        return 'post';
    }

    public static function getPostsMatchKeyword($query, $page) {
        $from_record = $page * 10;
        $model = Yii::$app->db->createCommand(
            ' SELECT p.id as id, title, content, p.image as image, time, privacy_id, p.created_at ' .
            ' FROM post as p ' .
            ' WHERE title like :query '.
            ' GROUP  BY time DESC '.
            ' LIMIT :from_record, 10 '
        );
        $model->bindValues([':query' => '%' . $query . '%', ':from_record' => $from_record]);
        return $model->queryAll();
    }
}