<?php

namespace app\models;

use yii\db\ActiveRecord;

class ScheduleUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'schedule_user';
    }
}