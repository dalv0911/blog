<?php

namespace app\models;

use yii\db\ActiveRecord;

class MessageGroup extends ActiveRecord
{
    public static function tableName()
    {
        return 'message_group';
    }
}