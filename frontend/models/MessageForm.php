<?php

namespace frontend\models;

use app\models\GroupUser;
use app\models\Message;
use app\models\MessageGroup;
use yii\base\Model;

class MessageForm extends Model
{
    public $send_user_id;
    public $receive_user_ids;
    public $group_name;
    public $content;
    public $sent_at;

    private $group_id;

    public function rules()
    {
        return [
            [['send_user_id', 'receive_user_ids', 'group_name', 'content'], 'required']
        ];
    }

    public function create()
    {
        $message_group = new MessageGroup();
        $message_group['name'] = $this->group_name;
        if ($message_group->save()) {
            $this->group_id = $message_group['id'];
            foreach ($this->receive_user_ids as $user_id) {
                $group_user = new GroupUser();
                $group_user['message_group_id'] = $message_group['id'];
                $group_user['user_id'] = $user_id;
                $group_user->save();
            }
            $group_user = new GroupUser();
            $group_user['message_group_id'] = $message_group['id'];
            $group_user['user_id'] = $this->send_user_id;
            $group_user->save();

            $message = new Message();
            $message['send_user_id'] = $this->send_user_id;
            $message['received_group_id'] = $message_group['id'];
            $message['content'] = $this->content;
            $message['sent_at'] = $this->sent_at;
            $message->save();
        }
    }

    public function getMessageGroupID()
    {
        return $this->group_id;
    }
}