<?php

/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 11/9/2015
 * Time: 9:52 AM
 */
namespace frontend\models;

use app\models\Friend;
use app\models\Notification;
use Yii;

class FriendQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new FriendQuery();
        } else {
            return static::$query;
        }
    }

    public function get_all_friend($user_id, $group_id)
    {
        $query = Yii::$app->db->createCommand(
            ' SELECT received_request_user_id as id,CONCAT(first_name," ",last_name) as full_name,image,group_friend.name ' .
            ' FROM user LEFT JOIN friend ON user.id = received_request_user_id LEFT JOIN group_friend ON group_friend.id = group_id ' .
            ' WHERE send_request_user_id=:user_id AND is_accepted = 1 AND group_friend.id = :group_id'
        );
        $query->bindValues([':user_id' => $user_id, ':group_id' => $group_id]);
        return $query->queryAll();
    }

    public function get_friends($user_id)
    {
        $query = Yii::$app->db->createCommand(
            ' SELECT received_request_user_id as id,CONCAT(first_name," ",last_name) as full_name,image,group_friend.name as group_name ' .
            ' FROM user LEFT JOIN friend ON user.id = received_request_user_id LEFT JOIN group_friend ON group_friend.id = group_id ' .
            ' WHERE send_request_user_id=:user_id AND is_accepted = 1'
        );
        $query->bindValues([':user_id' => $user_id]);
        return $query->queryAll();
    }

    public function accept_friend($notify_id)
    {
        $notification = Notification::findOne(['id' => $notify_id]);
        $send_request_user_id = $notification['from_user_id'];
        $received_request_user_id = $notification['to_user_id'];
        $friend = Friend::findOne(['send_request_user_id' => $send_request_user_id, 'received_request_user_id' => $received_request_user_id]);
        $friend->is_accepted = true;
        $friend->save();
        $new_friend = new Friend();
        $new_friend['send_request_user_id'] = $received_request_user_id;
        $new_friend['received_request_user_id'] = $send_request_user_id;
        $new_friend['group_id'] = $friend['group_id'];
        $new_friend['is_accepted'] = true;
        return $new_friend->save();
    }

}