<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 10:00 AM
 */
namespace app\models;

use yii\db\ActiveRecord;

class PostTag extends ActiveRecord
{
    public static function tableName()
    {
        return 'post_tag';
    }
}