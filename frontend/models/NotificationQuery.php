<?php
namespace frontend\models;

use app\models\Notification;
use DateTime;
use Yii;

/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/24/2015
 * Time: 12:33 PM
 */
class NotificationQuery
{
    public static $query;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (static::$query == null) {
            return new NotificationQuery();
        } else {
            return static::$query;
        }
    }

    /**
     * @param $from_user_id
     * @param $to_user_id
     * @param $type
     * type:1 - send request make friend
     * type:2 - accept request
     * @return bool
     */
    public function send_notification($from_user_id, $to_user_id, $type)
    {
        $model = new Notification();
        $model->from_user_id = $from_user_id;
        $model->to_user_id = $to_user_id;
        $model->type = $type;
        $model->date_time = (new DateTime())->format('y-m-d h:i:s');
        return $model->save();
    }

    public function get_total_number_notification($user_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT COUNT(id) as count' .
            ' FROM notification' .
            ' WHERE to_user_id =:user_id'
        );
        $model->bindValues([':user_id' => $user_id]);
        return $model->queryOne()['count'];
    }

    public function get_all_notification($to_user_id)
    {
        $model = Yii::$app->db->createCommand(
            ' SELECT n.id,from_user_id,CONCAT(first_name,"",last_name) as full_name,type,image as avatar,date_time' .
            ' FROM user LEFT JOIN notification as n ON from_user_id = user.id' .
            ' WHERE to_user_id=:to_user_id'
        );
        $model->bindValues([':to_user_id' => $to_user_id]);
        return $model->queryAll();
    }
}