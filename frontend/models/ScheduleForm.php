<?php

namespace frontend\models;

use yii\base\Model;

class ScheduleForm extends Model
{
    public $subject;
    public $content;
    public $address;
    public $from;
    public $to;
    public $owner_id;
    public $color;


    public function rules()
    {
        return [
            [['subject', 'content', 'address', 'from', 'to', 'color'], 'required'],
        ];
    }
}