<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/23/2015
 * Time: 1:10 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tìm kiếm người dùng';
$this->params['breadcrumbs'][0] = "Tìm kiếm";
$this->params['breadcrumbs'][1] = $this->title;
?>
<div class="row">
    <div class="col-lg-8">
        <form action="?r=search/user" method="post" class="form-horizontal">
            <div class="input-group input-group-sm">
                <input placeholder="Nhập tên người dùng" type="text" name="keyword" class="form-control"
                       value="<?= empty($keyword) ? '' : $keyword ?>" style="height: 50px;">
                <span class="input-group-btn">
                    <button class="btn btn-info btn-flat" type="submit" style="height: 50px;">Go!</button>
                </span>
            </div>
        </form>
        <br><br>
        <?php
        if (isset($results)) {
            if (empty($results)) {
                echo '<div class="callout callout-danger">
                    <h4>Không có kết quả nào phù hợp - Not found</h4>
                    </div>';
            }
            ?>
            <div class="box-body chat" id="chat-box">
                <?php
                $index = 0;
                $count = count($results);
                foreach ($results as $user) {
                    ?>
                    <!-- chat item -->
                    <div class="item message_item<?= $user['id'] ?>" style="height: 60px;background-color: #eee;">
                        <?php
                        if (!empty($user['image'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user['image'],
                                [
                                    'alt' => $user['full_name'],
                                    'class' => 'online',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                [
                                    'alt' => $user['full_name'],
                                    'class' => 'online',
                                ]
                            );
                        }
                        ?>
                        <p class="message">
                            <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $user['id']]) ?>"
                               class="name">
                                <?= $user['full_name'] ?>
                            </a>

                        <div style="margin-left:55px">
                            <?= $user['address'] ?>
                        </div>
                        </p>
                    </div>
                    <!-- /.item -->
                    <?php
                }
                ?>
            </div>

            <?php
            if (isset($results)) {
                if ($count >= 5) {
                    echo '<div class="box-footer clearfix">';
                    echo '<ul class="pagination pagination-sm no-margin pull-right">';

                    if ($page > 0) {
                        echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => ($page - 1)]) . '">Pre</a></li>';
                        echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => $page]) . '">' . ($page) . '</a></li>';
                    }
                    if ($count == 5) {
                        echo '<li><a href="' . Url::to(['search/user-result', 'keyword' => $keyword, 'page' => ($page + 1)]) . '">Next</a></li>';
                    }

                    echo '</ul>';
                    echo '</div>';
                }

            }
        }
        ?>
    </div>
</div>