<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 5:49 AM
 */
use dosamigos\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="post-form">
    <?php $form = ActiveForm::begin(
        [
            'id' => 'product-from',
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*', 'maxSize' => 10097152]) ?>
            <br>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'time')->widget(DatePicker::className(), [
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => $model['time'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]) ?>

                </div>
                <div class="col-md-6">
                    <label>Chủ đề</label>
                    <?= /** @var String[] $tags */
                    Select2::widget([
                        'name' => 'PostForm[tags][]',
                        'value' => $model->selected_tags,
                        'data' => $tags,
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'maximumInputLength' => 30
                        ],
                    ]);
                    ?>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-6">
                    <label>Mức độ riêng tư </label>
                    <br>
                    <label>
                        <select name="PostForm[privacy_id]" class="form-control">
                            <option value="1" <?= ($model->privacy_id == 1) ? 'selected' : '' ?>>Chỉ mình tôi</option>
                            <option value="2" <?= ($model->privacy_id == 2) ? 'selected' : '' ?>>Lựa chọn 1 số bạn bè
                            </option>
                            <option value="3" <?= ($model->privacy_id == 3) ? 'selected' : '' ?>>Chỉ tôi và bạn bè
                            </option>
                            <option value="4" <?= ($model->privacy_id == 4) ? 'selected' : '' ?>>Tất cả mọi người
                            </option>
                        </select>
                    </label>
                </div>
                <div class="col-md-6">
                    <label>Bình luận</label>
                    <br>

                    <div class="row">
                        <div class="radio">
                            <label>
                                <input type="radio" name="PostForm[comment]"
                                       value="1" <?= ($model->comment == true) ? 'checked' : '' ?>>
                                Có
                            </label>
                            <label>
                                <input type="radio" name="PostForm[comment]"
                                       value="0" <?= ($model->comment == true) ? '' : 'checked' ?>>
                                Không
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            <div class="form-group">
                <a class="btn btn-default show_message_form" href="<?= Yii::$app->request->referrer ?>"><i
                        class="fa fa-reply"></i> Cancel</a>
                <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'setting-button']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
