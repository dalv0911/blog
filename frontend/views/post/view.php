<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 12:21 PM
 */
use yii\helpers\Html;

$post = $model;
$relative_posts = $models;
$this->title = $post['title'];
$this->params['breadcrumbs'][0] = "Danh sách nhật ký";

$user_id = Yii::$app->user->getId();
$user_static = \common\models\User::findIdentity($user_id);
?>
<div class="row">
    <div class="col-lg-8">
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="user-block">
                    <?php
                    if (!empty($post['avatar'])) {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/' . $post['avatar'],
                            [
                                'alt' => "Avatar",
                                'class' => 'img-circle',
                            ]
                        );

                    } else {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                            [
                                'alt' => "Avatar",
                                'class' => 'img-circle',
                            ]
                        );
                    }
                    ?>
                    <span class="username"><a
                            href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $post['owner_id']]) ?>">
                            <?= empty($post['full_name']) ? 'No Name' : $post['full_name'] ?></a></span>
                    <span
                        class="description"><?= \frontend\utils\Helper::print_privacy($post['privacy_id']) . ' - ' . \frontend\utils\Helper::calculate_time($post['created_at']) ?></span>
                </div>
                <!-- /.user-block -->
                <?php
                if ($post['owner_id'] == $user_id || \frontend\utils\Helper::is_admin()) {
                    ?>
                    <div class="box-tools">
                        <a href="<?= \yii\helpers\Url::to(['post/edit', 'id' => $post['id']]) ?>"
                           class="btn btn-box-tool"
                           title="Chỉnh sửa"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-box-tool delete-post"
                           title="Xóa" id="<?= $post['id'] ?>"><i class="fa fa-trash"></i></a>
                    </div>
                    <?php
                }
                ?>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <li class="time-label">
                             <span class="bg-red">
                                <?= $post['time'] ?>
                            </span>
                </li>
                <p><?= $post['content'] ?></p>
            </div>
            <!-- /.box-body -->
            <div class="box-footer box-comments" id="box-comment">
                <?php
                /** @var Array[] $comments */
                foreach ($comments as $comment) {
                    ?>
                    <!-- /.box-comment -->
                    <div class="box-comment">
                        <!-- User image -->
                        <?php
                        if (!empty($post['avatar'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $comment['avatar'],
                                [
                                    'alt' => "Avatar",
                                    'class' => 'img-circle img-sm',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                [
                                    'alt' => "Avatar",
                                    'class' => 'img-circle img-sm',
                                ]
                            );
                        }
                        ?>
                        <div class="comment-text">
                      <span class="username">
                        <?= empty($comment['full_name']) ? 'No name' : $comment['full_name'] ?>
                          <?php
                          if ($comment['owner_id'] == $user_id) {
                              echo '<a class="del_comment text-muted pull-right" id="' . $comment['id'] . '"><i
                                      class="fa fa-trash"></i></a>
                                    <br>';
                          }
                          ?>
                          <span class="text-muted pull-right"><?= $comment['created_at'] ?></span>
                      </span><!-- /.username -->
                            <?= $comment['content'] ?>
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->
                    <?php
                }
                ?>
            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
                <!--                <form action="" method="post" class="comment-form">-->
                <?php
                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                    [
                        'alt' => "photo",
                        'class' => 'img-responsive img-circle img-sm',
                    ]
                );
                ?>
                <div class="img-push">
                    <input type="text" id="owner_id=<?= $user_id ?>&post_id=<?= $post['id'] ?>"
                           class="content_cmt form-control input-sm"
                           placeholder="Press enter to post comment" <?= $post['commentable'] == 0 ? 'disabled' : '' ?>>
                </div>
                <!-- .img-push is used to add margin to elements next to floating images -->
                <!--                </form>-->
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-lg-4">
        <?php
        /** @var Array[] $posts */
        foreach ($relative_posts as $post) {
            ?>
            <ul class="timeline">

                <!-- timeline time label -->
                <li class="time-label">
                             <span class="bg-red">
                                <?= $post['time'] ?>
                            </span>
                </li>
                <!-- /.timeline-label -->

                <!-- timeline item -->
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-clock-o bg-gray-light"></i>

                    <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i>
                                    <?= \frontend\utils\Helper::calculate_time($post['created_at']) ?>
                                </span>

                        <h3 class="timeline-header">
                            <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post['id']]) ?>">
                                <?= $post['title'] ?></a>
                        </h3>

                        <div class="timeline-body">
                            <?= str_split($post['content'], 300)[0] ?>
                        </div>
                    </div>
                </li>
            </ul>
            <?php
        }
        ?>
    </div>
</div>