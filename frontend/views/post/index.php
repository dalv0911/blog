<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 10:54 AM
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Danh sách nhật ký';
$this->params['breadcrumbs'][0] = "Quản lý nhật kí";
$this->params['breadcrumbs'][1] = $this->title;
/** @var Array[] $posts */
$count = count($posts);
?>
<div class="row">
    <span class="col-lg-2">
        <a href="?r=post/create" class="btn btn-block btn-success btn-md"><i class="fa fa-edit"> Viết bài mới</i></a>
    </span>
</div>
<br>

<?php
/** @var Array[] $posts */
foreach ($posts as $post) {
    ?>
    <ul class="timeline">

        <!-- timeline time label -->
        <li class="time-label">
                             <span class="bg-red">
                                <?= $post['time'] ?>
                            </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-clock-o bg-gray-light"></i>

            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i>
                                    <?= \frontend\utils\Helper::calculate_time($post['created_at']) ?>
                                </span>

                <h3 class="timeline-header">
                    <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post['id']]) ?>">
                        <?= $post['title'] ?></a>
                </h3>

                <div class="timeline-body">
                    <?= str_split($post['content'], 300)[0] ?>
                </div>
            </div>
        </li>
    </ul>
    <?php
}
?>

<?php
if ($count >= 10) {
    echo '<div class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right">';

    if ($page > 0) {
        echo '<li><a href="' . Url::to(['post/list', 'page' => ($page - 1)]) . '">Pre</a></li>';
        echo '<li><a href="' . Url::to(['post/list', 'page' => $page]) . '">' . ($page) . '</a></li>';
    }
    if ($count == 5) {
        echo '<li><a href="' . Url::to(['post/list', 'page' => ($page + 1)]) . '">Next</a></li>';
    }

    echo '</ul>';
    echo '</div>';
}
?>
