<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 10:55 AM
 */
use kartik\checkbox\CheckboxX;
use yii\helpers\Html;

$this->title = 'Lựa chọn bạn bè xem nhật ký';
$this->params['breadcrumbs'][0] = "Quản lý nhật kí";
$this->params['breadcrumbs'][1] = $this->title;
?>
<?php
\yii\widgets\ActiveForm::begin();
?>
<div class="box">
    <div class="box-body">
        <table class="table table-hover table-striped">
            <tbody>
            <?php
            /** @var Array[] $friends */
            foreach ($friends as $friend) {
                ?>
                <tr>
                    <td>
                        <div class="col-md-1">
                            <?php
                            $value = 0;
                            /** @var Array[] $selected_friends */
                            foreach ($selected_friends as $user) {
                                if ($friend['id'] == $user['user_id'])
                                    $value = 1;
                            }
                            echo CheckboxX::widget([
                                'name' => 'selected_friend[]',
                                'options' => ['id' => $friend['id'], 'class' => 'select_friend'],
                                'value' => $value,
                                'pluginOptions' => ['threeState' => false]
                            ]);
                            ?>
                        </div>
                        <div class="user-block">
                            <?php
                            if (!empty($friend['image'])) {
                                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $friend['image'],
                                    [
                                        'alt' => "Avatar",
                                        'class' => 'attachment-img',
                                    ]
                                );

                            } else {
                                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                    [
                                        'alt' => "Avatar",
                                        'class' => 'attachment-img',
                                    ]
                                );
                            }
                            ?>
                            <span class="username" style="padding-left: 90px;">
                            <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $friend['id']]) ?>">
                                <?= $friend['full_name'] ?>
                            </a>
                            </span>
                            <span
                                class="description"><?= $friend['group_name'] ?>
                            </span>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <br>
        <?php
        if (!empty($friends)) {
            ?>
            <div class="form-group">
                <?= Html::submitButton('Lưu', ['class' => 'btn btn-success', 'name' => 'send-button']) ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<?php
\yii\widgets\ActiveForm::end();
?>
