<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/24/2015
 * Time: 1:31 PM
 */
use yii\helpers\Html;

$this->title = 'Thông báo';
$this->params['breadcrumbs'][0] = $this->title;
?>
<?php
/** @var Array[] $notifications */
foreach ($notifications as $notify) {
    ?>
    <div class="box box-widget col-lg-6">
    <div class="box-header with-border">
        <div class="user-block">
            <?php
            if (empty($notify['avatar'])) {
                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                    [
                        'alt' => "Avatar",
                        'class' => 'img-circle',
                    ]
                );
            } else {
                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $notify['avatar'],
                    [
                        'alt' => "Avatar",
                        'class' => 'img-circle',
                    ]
                );
            }
            ?>
            <div class="notifications-right">
                <span class="username"><a
                        href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $notify['from_user_id']]) ?>">
                        <?= empty($notify['full_name']) ? 'Chưa đặt tên' : $notify['full_name'] ?></a></span>
                    <span class="description">
                    <?php
                    if ($notify['type'] == \frontend\utils\Helper::$SEND_REQUEST_MAKE_FRIEND) {
                        echo '<p>Đã gửi cho bạn một yêu cầu kết bạn</p>';
                        echo \frontend\utils\Helper::calculate_time($notify['date_time']) . '<br>';
                        echo '<a class="accept_friend" id="' . $notify['id'] . '"><i class="fa fa-user-plus"></i> Đồng ý</a>';
                        echo '<a class="refuse_friend" id="' . $notify['id'] . '"><i class="fa fa-user-times"></i> Từ chối</a>';
                    } else {
                        echo '<p>Đã chấp nhận yêu cầu kết bạn của bạn</p>';
                        echo \frontend\utils\Helper::calculate_time($notify['date_time']) . '<br>';
                        echo '<a class="del_notify" id="' . $notify['id'] . '"><i class="fa fa-trash"></i>Xóa thông báo</a>';
                    }
                    ?>
                    </span>
            </div>
            <!-- /.user-block -->
        </div>
    </div>
    <?php
}
?>