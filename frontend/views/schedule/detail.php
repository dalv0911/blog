<?php

use yii\helpers\Html;

$this->title = 'Chi tiết lịch';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary" style="border-color: <?= $schedule['color'] ?>">
            <div class="box-header with-border">
                <h3 class="box-title">Lịch của bạn</h3>

                <div class="box-tools">
                    <a href="<?= \yii\helpers\Url::to(['schedule/edit', 'id' => $schedule['id']]) ?>"
                       class="btn btn-box-tool" title="Chỉnh sửa"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-box-tool delete-schedule" id="<?= $schedule['id'] ?>"
                       title="Xóa"><i class="fa fa-trash"></i></a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                if ($schedule['owner_id'] != Yii::$app->user->getId()) {
                ?>
                <strong><i class="fa fa-user"></i> Người gửi</strong>
            <br>
                <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $schedule['owner_id']]) ?>">
                    <?php
                    if (!empty($schedule['image'])) {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/' . $schedule['image'],
                            [
                                'title' => $schedule['full_name'],
                                'class' => 'img-circle',
                                'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                            ]
                        );

                    } else {
                        echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                            [
                                'title' => $schedule['full_name'],
                                'class' => 'img-circle',
                                'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                            ]
                        );
                    }
                    }
                    ?>
                </a>

                <strong><i class="fa fa-object-group"></i> Chủ đề</strong>

                <p class="text-muted">
                    <?= $schedule['subject'] ?>
                </p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Nội dung</strong>

                <p class="text-muted"><?= $schedule['content'] ?></p>

                <hr>

                <strong><i class="fa fa-anchor"></i> Địa điểm</strong>

                <p><?= $schedule['address'] ?></p>

                <hr>

                <strong><i class="fa fa-calendar"></i> Thời gian</strong>

                <p>Từ: <?= $schedule['from'] ?></p>

                <p>Đến: <?= $schedule['to'] ?></p>

                <div class="box-footer clearfix">
                    <a href="?r=schedule/index" class="pull-left btn btn-default"><i
                            class="fa fa-arrow-circle-left"></i> Quay trở lại</a>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
