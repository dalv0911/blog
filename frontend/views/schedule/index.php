<?php

use kartik\color\ColorInput;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;

$this->title = 'Lịch làm việc';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (isset($edited)) {
    ?>
    <div class="callout callout-success">
        <h4>Chỉnh sửa lịch làm việc thành công!</h4>

        <p>Bạn đã chỉnh sửa lịch làm việc thành công.</p>
    </div>
    <?php
}
?>
<div class="row">
    <div class="col-lg-3">
        <div class="box box-solid">
            <div class="box-header with-border">
                <?php
                if (isset($edit)) {
                    ?>
                    <h3 class="box-title">Chỉnh sửa lịch làm việc</h3>
                    <?php
                } else {
                    ?>
                    <h3 class="box-title">Tạo mới lịch làm việc</h3>
                    <?php
                }
                ?>
            </div>
            <?php
            $form = \yii\widgets\ActiveForm::begin();
            ?>
            <div class="input" style="padding: 10px 10px 10px 10px;">
                <?= $form->field($model, 'subject')->textInput(['placeholder' => 'Enter subject']) ?>

                <?= $form->field($model, 'content')->textarea(['placeholder' => 'Enter content']) ?>

                <?= $form->field($model, 'address')->textInput(['placeholder' => 'Enter address']) ?>

                <?= $form->field($model, 'from')->widget(DateTimePicker::className(), [
                    'options' => ['placeholder' => 'From'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd  hh:ii:ss'
                    ],
                ]) ?>

                <?= $form->field($model, 'to')->widget(DateTimePicker::className(), [
                    'options' => ['placeholder' => 'To'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ],
                ]) ?>

                <?= $form->field($model, 'color')->widget(ColorInput::className(), [
                    'value' => '#00ff00',
                    'options' => ['placeholder' => 'Color'],
                    'attribute' => 'saturation',
                ]) ?>

                <br>
                <?= Html::submitButton('New', ['class' => 'btn btn-success', 'name' => 'new-schedule']) ?>
            </div>
            <?php
            \yii\widgets\ActiveForm::end();
            ?>
        </div>
    </div>
    <div class="col-lg-9">
        <?php
        /** @var Array[] $events */
        echo \yii2fullcalendar\yii2fullcalendar::widget(array(
            'events' => $events,
        ));
        ?>
    </div>
</div>