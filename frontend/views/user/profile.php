<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/19/2015
 * Time: 8:31 PM
 */
use yii\helpers\Url;
use yii\helpers\Html;

$user = $model;
$this->title = empty($user['full_name']) ? $user['username'] : $user['full_name'];
$this->params['breadcrumbs'][0] = "Danh sách thành viên";
$this->params['breadcrumbs'][1] = $this->title;
?>
<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?php
                if (!empty($user['image'])) {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user['image'],
                        [
                            'alt' => $user['full_name'],
                            'class' => 'img-circle profile-user-img img-responsive',
                            'style' => 'width:70px;height:70px;border-radius: 50%;border: 2px solid #dd4b39;',
                        ]
                    );

                } else {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                        [
                            'alt' => $user['full_name'],
                            'class' => 'img-circle profile-user-img img-responsive',
                            'style' => 'width:70px;height:70px;border-radius: 50%;border: 2px solid #dd4b39;',
                        ]
                    );
                }
                ?>
                <h3 class="profile-username text-center">
                    <a href="<?= Url::to(['user/profile', 'id' => $user['id']]) ?>">
                        <?= empty($user['full_name']) ? $user['username'] : $user['full_name'] ?>
                    </a>
                </h3>

                <p class="text-muted text-center"><?= $user['level_id'] == 1 ? 'Administrator' : 'Member' ?></p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Số bài đăng</b> <a class="pull-right"><?= $user['prod_num'] ?></a>
                    </li>
                    <?php
                    //                    * 1.No relative
                    //                    * 2.Cho accept
                    //                    * 3.has request
                    //                    * 4.Friend

                    /** @var int $relative_code */
                    if (isset($relative_code)) {
                        switch ($relative_code) {
                            case 1:
                                echo '<a id ="' . $user['id'] . '"class="add_friend">' .
                                    '<li class="list-group-item">' .
                                    '    <i class="fa fa-user-plus"></i> Thêm bạn bè</li>' .
                                    '</a>';
                                break;
                            case 2:
                                echo ' <a class="list-group-item">' .
                                    '    <i class="fa fa-mail-forward"></i> Đã gửi yêu cầu' .
                                    ' </a>';
                                break;
                            case 3:
                                echo '<a href="?r=user/accept-friend&user_id=' . $user['id'] . '">' .
                                    '<li class="list-group-item">' .
                                    '    <i class="fa  fa-check-square-o"></i> Xác nhận bạn bè </li>' .
                                    '</a>';
                                break;
                            case 4:
                                echo '<a class="list-group-item">' .
                                    '    <i class="fa fa-user-plus"></i> Bạn bè' .
                                    '</a>';
                                break;
                            default:
                                break;
                        }
                    }
                    ?>
                    <a class="list-group-item" style="display: none" id="sent_request_friend">
                        <i class="fa fa-mail-forward"></i> Đã gửi yêu cầu
                    </a>
                    <a class="list-group-item" style="display: none" id="is_friend">
                        <i class="fa fa-mail-forward"></i> Friend
                    </a>

                    <div class="list-group-item" style="display: none" id="choice_group">
                        <form method="post" class="form-group">
                            <div class="radio">
                                <label>
                                    <input name="group_friend" id="optionsRadios1" value="1" type="radio">
                                    Người thân
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="group_friend" id="optionsRadios2" value="2" checked type="radio">
                                    Bạn bè
                                </label>
                            </div>
                            <button type="submit" class="btn btn-success" name="add_friend">Make Friend</button>
                        </form>
                    </div>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa fa-envelope"></i> Email</strong>

                <p class="text-muted">
                    <?= $user['email'] ?>
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Địa chỉ</strong>

                <p class="text-muted"><?= $user['address'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Sở thích</strong>

                <p><?= $user['hobby'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Công việc</strong>

                <p><?= $user['work'] ?></p>

                <hr>

                <strong><i class="fa fa-file-text-o margin-r-5"></i> Note</strong>

                <p><?= $user['desc'] ?></p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php if ($user['id'] == Yii::$app->user->getId()) { ?>
                    <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Settings</a></li>
                    <li class=""><a href="#friends" data-toggle="tab" aria-expanded="false" aria-selected="true">Bạn
                            bè</a></li>
                <?php } ?>
                <li class=""><a href="#activity" data-toggle="tab" aria-expanded="false" aria-selected="true">Hoạt động
                        gần đây</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="activity">
                    <?php
                    /** @var Array[] $posts */
                    foreach ($posts as $post) {
                        ?>
                        <ul class="timeline">

                            <!-- timeline time label -->
                            <li class="time-label">
                             <span class="bg-red">
                                <?= $post['time'] ?>
                            </span>
                            </li>
                            <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                <i class="fa fa-clock-o bg-gray-light"></i>

                                <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i>
                                    <?= \frontend\utils\Helper::calculate_time($post['created_at']) ?>
                                </span>

                                    <h3 class="timeline-header">
                                        <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post['id']]) ?>">
                                            <?= $post['title'] ?></a>
                                    </h3>

                                    <div class="timeline-body">
                                        <?= str_split($post['content'], 300)[0] ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
                <div class="tab-pane active" id="settings">
                    <?php
                    if (isset($isUpdated)) {
                        if ($isUpdated) {
                            ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-check"></i> Chúc mừng!</h4>
                                Bạn đã chỉnh sửa thông tin thành công.
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Thật đáng tiếc!</h4>
                                Thông tin cá nhân của bạn hiện chưa được chỉnh sửa.
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    if ($user['id'] == Yii::$app->user->getId()) {
                        echo $this->render('_form', ['model' => $setting]);
                    }
                    ?>
                </div>

                <div class="tab-pane" id="friends">

                    <?php if (empty($friendList) && empty($familyList)) { ?>
                        <div class="callout callout-info">
                            <h4>Bạn chưa có bạn bè, người thân!</h4>
                        </div>
                    <?php } ?>

                    <?php if (!empty($friendList)) { ?>
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bạn bè</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <ul class="users-list clearfix">
                                    <?php
                                    foreach ($friendList as $friend) {
                                        ?>
                                        <li>
                                            <?php
                                            if (!empty($friend['image'])) {
                                                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $friend['image'],
                                                    [
                                                        'alt' => "Avatar",
                                                        'class' => 'img-circle',
                                                        'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                                                    ]
                                                );

                                            } else {
                                                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                                    [
                                                        'alt' => "Avatar",
                                                        'class' => 'img-circle',
                                                        'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                                                    ]
                                                );
                                            }
                                            ?>
                                            <a class="users-list-name"
                                               href="<?= Url::to(['user/profile', 'id' => $friend['id']]) ?>"><span
                                                    style="font-size: 15px"> <?= $friend['full_name'] ?></span></a>
                                            <!--                                <span class="users-list-date">Today</span>-->
                                        </li>
                                    <?php } ?>
                                </ul>
                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    <?php } ?>

                    <?php if (!empty($familyList)) { ?>
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Người thân</h3>

                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <ul class="users-list clearfix">
                                    <?php
                                    foreach ($familyList as $member) {
                                        ?>
                                        <li>
                                            <?php
                                            if (!empty($member['image'])) {
                                                echo Html::img(Yii::$app->request->baseUrl . '/images/' . $member['image'],
                                                    [
                                                        'alt' => "Avatar",
                                                        'class' => 'img-circle',
                                                        'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                                                    ]
                                                );

                                            } else {
                                                echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                                    [
                                                        'alt' => "Avatar",
                                                        'class' => 'img-circle',
                                                        'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                                                    ]
                                                );
                                            }
                                            ?>
                                            <a class="users-list-name"
                                               href="<?= Url::to(['user/profile', 'id' => $member['id']]) ?>"><span
                                                    style="font-size: 15px"> <?= $member['full_name'] ?></span></a>
                                            <!--                                <span class="users-list-date">Today</span>-->
                                        </li>
                                    <?php } ?>
                                </ul>
                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>