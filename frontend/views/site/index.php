<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Nhật ký trực tuyến';
?>
<div class="site-index">
    <ul class="timeline">
        <?php
        if (!empty($posts)) {
            foreach ($posts as $post) {
                ?>
                <li class="time-label">
                             <span class="bg-red">
                                 <?= $post['time'] ?>
                            </span>
                </li>
                <li>
                    <i class="fa fa-clock-o bg-gray-light"></i>

                    <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i>
                                </span>

                        <h3 class="timeline-header">
                            <span class="user-block">
                                <?php
                                if (!empty($post['avatar'])) {
                                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $post['avatar'],
                                        [
                                            'alt' => $post['full_name'],
                                            'class' => 'online',
                                        ]
                                    );

                                } else {
                                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                        [
                                            'alt' => $post['full_name'],
                                            'class' => 'online',
                                        ]
                                    );
                                }
                                ?>
                                <span class="username"><a
                                        href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $post['owner_id']]) ?>"><?= $post['full_name'] ?></a></span>
                                <span
                                    class="description"><?= \frontend\utils\Helper::print_privacy($post['privacy_id']) ?>
                                    - <?= \frontend\utils\Helper::calculate_time($post['created_at']) ?></span>
                            </span>
                            <!-- /.user-block -->
                        </h3>

                        <div class="timeline-body">
                            <div class="box box-widget">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <a href="<?= \yii\helpers\Url::to(['post/view', 'id' => $post['id']]) ?>"><strong><?= $post['title'] ?></strong></a>
                                    <?= str_split($post['content'], 500)[0] ?>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</div>
