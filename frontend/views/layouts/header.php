<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$user_id = Yii::$app->user->getId();
$user_static = \common\models\User::findIdentity($user_id);
$notification_query = \frontend\models\NotificationQuery::getInstance();
?>
<header class="main-header">
    <nav class="navbar navbar-static-top" style="margin-left: 0;">
        <div>
            <div class="navbar-header">
                <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand"><b>Nhật ký</b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php
                    if (!Yii::$app->user->isGuest) {
                        ?>
                        <li><a href="?r=post">Danh sách bài đăng</a></li>
                        <?php
                    }
                    ?>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="?r=search/user">Tìm kiếm người dùng</a></li>

                </ul>
                <form class="navbar-form navbar-left" role="search" action="?r=search/post" method="post">
                    <div class="form-group">
                        <input type="text" name="keyword" class="form-control" id="navbar-search-input"
                               placeholder="Tìm nhật ký">
                    </div>
                </form>
            </div>
            <!-- /.navbar-collapse -->
            <?php
            if (!Yii::$app->user->isGuest) {
                ?>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="?r=message" title="Tin nhắn">
                                <i class="fa fa-envelope-o"></i>
                                <span class="label label-success"></span>
                            </a>
                        </li>
                        <!-- /.messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <a href="?r=notify/index" class="dropdown-toggle" title="Thông báo">
                                <i class="fa fa-bell-o"></i>
                                <span
                                    class="label label-warning"><?= $num_notify = $notification_query->get_total_number_notification($user_id) ?>
                                </span>
                            </a>
                        </li>
                        <li class="dropdown messages-menu">
                            <a href="?r=schedule" title="Lịch làm việc">
                                <i class="fa fa-calendar-plus-o"></i>
                                <span class="label label-success"></span>
                            </a>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php
                                if (!empty($user_static['image'])) {
                                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                                        [
                                            'alt' => "Avatar",
                                            'class' => 'user-image',
                                        ]
                                    );

                                } else {
                                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                        [
                                            'alt' => "Avatar",
                                            'class' => 'user-image',
                                        ]
                                    );
                                }
                                ?>
                                <span class="hidden-xs">
                             <?php
                             $full_name_static = $user_static['first_name'] . ' ' . $user_static['last_name'];
                             echo empty($full_name_static) ? $user_static['username'] : $full_name_static;
                             ?>
                        </span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <?php
                                    if (!empty($user_static['image'])) {
                                        echo Html::img(Yii::$app->request->baseUrl . '/images/' . $user_static['image'],
                                            [
                                                'alt' => "Avatar",
                                                'class' => 'img-circle',
                                            ]
                                        );

                                    } else {
                                        echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                            [
                                                'alt' => "Avatar",
                                                'class' => 'img-circle',
                                            ]
                                        );
                                    }
                                    ?>
                                    <p>
                                        <?php
                                        $full_name_static = $user_static['first_name'] . ' ' . $user_static['last_name'];
                                        echo empty($full_name_static) ? $user_static['username'] : $full_name_static;
                                        ?>
                                        <small><?= $user_static['level_id'] == 1 ? 'Administrator' : 'Member ' ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => Yii::$app->user->getId()]) ?>"
                                           class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Sign out',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-custom-menu -->
                <?php
            } else {
                ?>
                <div class="collapse navbar-collapse pull-right">
                    <ul class="nav navbar-nav">
                        <li><a href="?r=site/login">Đăng nhập</a></li>
                        <li><a href="?r=site/signup">Đăng ký</a></li>
                    </ul>
                </div>
                <?php
            }
            ?>
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>