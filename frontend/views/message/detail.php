<?php
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;

?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h2 class="box-title" style="font-weight: bold"><?= $message['subject'] ?></h2>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="mailbox-read-info">
            <?php
            foreach ($message['members'] as $member) {
                if (!empty($member['image'])) {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/' . $member['image'],
                        [
                            'title' => $member['full_name'],
                            'class' => 'img-circle',
                            'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                        ]
                    );

                } else {
                    echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                        [
                            'title' => $member['full_name'],
                            'class' => 'img-circle',
                            'style' => 'width:40px;height:40px;border-radius: 50%;border: 2px solid #dd4b39;',
                        ]
                    );
                }
            }
            ?>
        </div>
        <!-- Chat box -->
        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-comments-o"></i>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle">
                        <button type="button" class="btn btn-default btn-sm active"><i
                                class="fa fa-square text-green"></i></button>
                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="box-body chat" id="chat-box">
                <?php
                foreach ($message['messages'] as $mess) {
                    ?>
                    <?php
                    if ($mess['send_user_id'] == Yii::$app->user->getId()) {
                        $message_style = "background-color: #D1CACA;border-radius: 5px;";
                    } else {
                        $message_style = "border-radius: 5px;";
                    }
                    ?>
                    <!-- chat item -->
                    <div class="item message_item<?= $mess['mess_id'] ?>" style="<?= $message_style ?>">
                        <?php
                        if (!empty($mess['image'])) {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/' . $mess['image'],
                                [
                                    'alt' => $mess['full_name'],
                                    'class' => 'online',
                                ]
                            );

                        } else {
                            echo Html::img(Yii::$app->request->baseUrl . '/images/default.jpg',
                                [
                                    'alt' => $mess['full_name'],
                                    'class' => 'online',
                                ]
                            );
                        }
                        ?>
                        <p class="message">
                            <a href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $mess['send_user_id']]) ?>"
                               class="name">
                                <small class="text-muted pull-right"><i
                                        class="fa fa-clock-o"></i><?= \frontend\utils\Helper::calculate_time($mess['sent_at']) ?></small>
                                <?= $mess['full_name'] ?>
                            </a>
                            <?php
                            if ($mess['send_user_id'] == Yii::$app->user->getId()) {
                                ?>
                                <a class="text-muted pull-right delete_message_item" id="<?= $mess['mess_id'] ?>"
                                   style="margin-left: 10px"><i
                                        class="fa fa-trash"></i></a>
                                <?php
                            }
                            ?>
                        <div style="margin-left:55px">
                            <?= $mess['content'] ?>
                        </div>
                        </p>
                    </div>
                    <!-- /.item -->
                    <?php
                }
                ?>
            </div>
            <!-- /.box (chat box) -->
            <!-- /.mailbox-read-message -->
        </div>
        <!-- /.box-body -->
        <?php
        \yii\widgets\ActiveForm::begin();
        ?>
        <div class="reply_message_form" style="display: none;">
            <?= CKEditor::widget([
                'name' => 'reply_message_content',
                'options' => ['rows' => 4, 'class' => 'reply_message_content'],
                'preset' => 'basic',
            ]);
            ?>
        </div>
        <div class="box-footer">
            <div class="pull-right">
                <a class="btn btn-default show_message_form"><i class="fa fa-reply"></i> Reply</a>
                <button class="btn btn-default reply_message" type="submit" id="<?= $message['group_id'] ?>"
                        style="display: none"><i
                        class="fa fa-reply"></i> Reply
                </button>
            </div>
            <a class="btn btn-default leave_group_message" id="<?= $message['group_id'] ?>"><i class="fa fa-stop"></i>
                Leave</a>
        </div>
        <?php
        \yii\widgets\ActiveForm::end();
        ?>
        <!-- /.box-footer -->
    </div>
    <!-- /. box -->
</div>

