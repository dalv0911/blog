<?php
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Thư đã gửi</h3>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
                <tbody>
                <?php
                foreach ($messages as $message) {
                    ?>
                    <tr>
                        <!--                            <td><input type="checkbox"></td>-->
                        <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>

                        <td class="mailbox-subject"><b><a
                                    href="<?= \yii\helpers\Url::to(['message/detail', 'group_id' => $message['message_group_id']]) ?>"><?= $message['message_group_name'] ?></a></b>
                        </td>
                        <td class="mailbox-attachment"></td>
                        <?php
                        $sent_at = new DateTime($message['sent_at']);
                        $current = new DateTime();
                        $interval = $sent_at->diff($current);
                        $hour = $interval->format('%h');
                        $minutes = $interval->format('%i');
                        $day = $interval->format('%d');
                        $datetime = '';
                        if ($day > 0) {
                            $datetime = $day . " day ago";
                        } else if ($hour > 0) {
                            $datetime = $hour . " hour ago";
                        } else if ($minutes > 0) {
                            $datetime = $minutes . " minutes ago";
                        }
                        ?>
                        <td class="mailbox-date"><?= $datetime ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <!-- /.table -->
        </div>
        <!-- /.mail-box-messages -->
    </div>
    <!-- /.box-body -->
