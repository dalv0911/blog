<?php
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Soạn tin nhắn</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php
        $form = \yii\widgets\ActiveForm::begin();
        ?>
        <div class="form-group">
            <?= /** @var String[] $friends */
            Select2::widget([
                'name' => 'MessageForm[receive_user_ids][]',
                'options' => ['multiple' => true, 'placeholder' => 'Người Nhận'],
                'data' => $friends,
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 30,
                ],
            ]);
            ?>
        </div>
        <div class="form-group">
            <input class="form-control" name="MessageForm[group_name]" placeholder="Chủ đề:">
        </div>
        <div class="form-group">
            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
        </div>
        <a class="btn btn-default" href="?r=message"><i class="fa fa-times"></i> Discard</a>
    </div>
    <?php
    \yii\widgets\ActiveForm::end();
    ?>
    <!-- /.box-footer -->
</div><!-- /. box -->
