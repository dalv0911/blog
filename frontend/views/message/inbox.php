<?php
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Hộp thư đến</h3>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
                <tbody>
                <?php
                foreach ($messages as $message) {
                    ?>
                    <tr>
                        <!--                            <td><input type="checkbox"></td>-->
                        <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
                        <td class="mailbox-name"><a
                                href="<?= \yii\helpers\Url::to(['user/profile', 'id' => $message['send_user_id']]) ?>"><?= $message['full_name'] ?></a>
                        </td>
                        <td class="mailbox-subject"><b><a
                                    href="<?= \yii\helpers\Url::to(['message/detail', 'group_id' => $message['message_group_id']]) ?>"><?= $message['message_group_name'] ?></a></b>
                        </td>
                        <td class="mailbox-attachment"></td>

                        <td class="mailbox-date"><?= \frontend\utils\Helper::calculate_time($message['sent_at']) ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <!-- /.table -->
        </div>
        <!-- /.mail-box-messages -->
    </div>
    <!-- /.box-body -->
