<?php
$this->title = 'Tin nhắn';
$this->params['breadcrumbs'][0] = $this->title;
?>
<div class="row">
    <div class="col-md-3">
        <a href="?r=message/create" class="btn btn-primary btn-block margin-bottom">Soạn tin nhắn</a>

        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Thư mục</h3>

                <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li <?= isset($is_inbox) ? 'class="active"' : '' ?>>
                        <a href="?r=message/inbox"><i class="fa fa-inbox"></i> Hộp thư đến</a></li>
                    <li <?= isset($is_sent) ? 'class="active"' : '' ?>><a href="?r=message/sent"><i
                                class="fa fa-envelope-o"></i> Thư đã gửi</a></li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <?php
        if (isset($is_inbox)) {
            echo $this->render('inbox', ['messages' => $messages]);
        } else if (isset($is_sent)) {
            echo $this->render('sent', ['messages' => $messages]);
        } else if (isset($is_create)) {
            echo $this->render('create', ['model' => $model, 'friends' => $friends]);
        } else if (isset($is_detail)) {
            echo $this->render('detail', ['message' => $message]);
        }
        ?>
    </div>
    <!-- /.col -->
</div><!-- /.row -->
