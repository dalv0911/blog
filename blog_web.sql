-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2015 at 10:37 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `content`, `owner_id`, `post_id`, `created_at`) VALUES
(18, 'Bài viết này có thể comment :D', 1, 10, 'May 11, 2017 3:50:12 PM');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE IF NOT EXISTS `friend` (
  `id` int(11) NOT NULL,
  `send_request_user_id` int(11) NOT NULL,
  `received_request_user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `friend`
--

INSERT INTO `friend` (`id`, `send_request_user_id`, `received_request_user_id`, `group_id`, `is_accepted`) VALUES
(9, 1, 9, 1, 1),
(10, 9, 1, 1, 1),
(11, 1, 3, 2, 1),
(12, 3, 1, 2, 1),
(13, 1, 2, 2, 1),
(14, 2, 1, 2, 1),
(15, 1, 10, 2, 1),
(16, 10, 1, 2, 1),
(17, 1, 11, 2, 1),
(18, 11, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `group_friend`
--

CREATE TABLE IF NOT EXISTS `group_friend` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_friend`
--

INSERT INTO `group_friend` (`id`, `name`) VALUES
(1, 'Người thân'),
(2, 'Bạn bè');

-- --------------------------------------------------------

--
-- Table structure for table `group_user`
--

CREATE TABLE IF NOT EXISTS `group_user` (
  `id` int(11) NOT NULL,
  `message_group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_user`
--

INSERT INTO `group_user` (`id`, `message_group_id`, `user_id`) VALUES
(16, 5, 2),
(17, 6, 1),
(18, 6, 9),
(19, 7, 3),
(20, 7, 2),
(21, 7, 11),
(22, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL,
  `send_user_id` int(11) NOT NULL,
  `received_group_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `sent_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `send_user_id`, `received_group_id`, `content`, `sent_at`) VALUES
(11, 1, 5, '<p>Ch&agrave;o anh em trong nh&oacute;m nh&eacute;</p>\r\n', '2015-11-24 09:30:39'),
(15, 2, 5, '<p>Ch&agrave;o Đa, M&igrave;nh l&agrave; Tuấn nh&eacute;</p>\r\n', '2015-11-24 10:22:46'),
(16, 9, 6, '<p>Anh ơi popup l&agrave;m kiểu g&igrave; nhỉ?</p>\r\n', '2015-11-29 10:24:25'),
(17, 1, 6, '<p>Anh kh&ocirc;ng biết l&agrave;m :&#39;(</p>\r\n', '2015-11-29 10:24:51'),
(18, 1, 7, '<p>Anh em cố gắng ho&agrave;n th&agrave;nh b&aacute;o c&aacute;o c&aacute;c phần trong ng&agrave;y h&ocirc;m nay nh&eacute;</p>\r\n', '2015-11-29 03:10:48'),
(19, 11, 7, '<p>Ok. Của t sắp xong rồi</p>\r\n', '2015-11-29 03:11:20'),
(20, 3, 7, '<p>Của t xong rồi nh&eacute;</p>\r\n', '2015-11-29 03:11:59'),
(21, 2, 7, '<p>Xong hết rồi nh&eacute;</p>\r\n', '2015-11-29 03:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `message_group`
--

CREATE TABLE IF NOT EXISTS `message_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_group`
--

INSERT INTO `message_group` (`id`, `name`) VALUES
(5, 'Akatsuki'),
(6, 'jQuery'),
(7, 'Viết báo cáo bài tập lớn');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `date_time` varchar(200) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `from_user_id`, `to_user_id`, `date_time`, `type`) VALUES
(6, 1, 7, 'Nov 11, 2015 4:22:57 PM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `privacy_id` int(11) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `commentable` tinyint(1) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `image`, `owner_id`, `privacy_id`, `created_at`, `updated_at`, `commentable`, `time`) VALUES
(8, 'ccknkcjnscjkn', '<p>nạkcnackjncjskna</p>\r\n', 'mau-cua-tinh-yeu-1.jpg', 3, 2, '0000-00-00 00:00:00.000000', NULL, 0, '26-Nov-2015'),
(10, 'Sao em lỡ vội lấy chồng', '<p>SAO EM NỠ VỘI LẤY CHỒNG&hellip;</p>\r\n\r\n<p>Hoa y&ecirc;u Qu&acirc;n khi c&ocirc; đang l&agrave; sinh vi&ecirc;n năm thứ ba của trường đại học. Qu&acirc;n lớn hơn Hoa 8 tuổi, anh đang l&agrave; nh&acirc;n vi&ecirc;n ph&aacute;p chế của một ng&acirc;n h&agrave;ng. Hoa l&agrave; một c&ocirc; g&aacute;i tỉnh lẻ với bố mẹ l&agrave;m c&ocirc;ng chức b&igrave;nh thường c&ograve;n Qu&acirc;n th&igrave; l&agrave; trai H&agrave; Nội &ldquo;xịn&rdquo;. Từ khi y&ecirc;u anh, Hoa cũng x&aacute;c định sẽ nỗ lực thật nhiều v&igrave; tương lai hai đứa. Qu&acirc;n cũng kh&ocirc;ng phụ l&ograve;ng c&ocirc;, d&ugrave; bận rộn nhưng anh rất cưng chiều v&agrave; lu&ocirc;n d&agrave;nh thời gian cho người y&ecirc;u. T&igrave;nh y&ecirc;u của hai người tưởng chừng sẽ m&atilde;i tốt đẹp như thế&hellip;</p>\r\n\r\n<p>Y&ecirc;u nhau hơn một năm th&igrave; Hoa ra trường. Vốn học giỏi, ngoại h&igrave;nh lại xinh xắn, Hoa dễ d&agrave;ng t&igrave;m được một vị tr&iacute; trong một c&ocirc;ng ty nước ngo&agrave;i. Rồi Qu&acirc;n đưa Hoa về gặp gia đ&igrave;nh, giới thiệu c&ocirc; với mọi người trong nh&agrave;. Hai người đ&atilde; hết sức mong đợi t&igrave;nh y&ecirc;u của họ sẽ kết tinh bằng một đ&aacute;m cưới đẹp như mơ. Khi Hoa về ra mắt, mọi người trong nh&agrave; đều c&oacute; vẻ qu&yacute; mến c&ocirc;, trừ mẹ của Qu&acirc;n. C&ocirc; biết mẹ Qu&acirc;n kh&ocirc;ng h&agrave;i l&ograve;ng về m&igrave;nh qua c&aacute;ch cư xử của b&agrave; trong buổi gặp gỡ đ&oacute;. B&agrave; ch&ecirc; giỏ qu&agrave; c&ocirc; mang tới rẻ tiền, kh&ocirc;ng xứng tầm với gia đ&igrave;nh b&agrave;. B&agrave; ch&ecirc; c&ocirc; nấu ăn kh&ocirc;ng vừa miệng, rồi giọng n&oacute;i của c&ocirc; đặc sệt &ldquo;th&ocirc;n qu&ecirc;&rdquo;. B&agrave; c&ograve;n b&oacute;ng gi&oacute; ch&ecirc; Hoa l&agrave; dạng con g&aacute;i &ldquo;đ&agrave;o mỏ&rdquo;, &ldquo;tinh mắt nh&igrave;n tr&uacute;ng c&aacute;i mỏ kim loại qu&yacute;&rdquo; l&agrave; con trai b&agrave;&hellip; Trước những lời n&oacute;i như x&aacute;t muối của &ldquo;mẹ chồng tương lai&rdquo;, Hoa buồn v&agrave; tự &aacute;i nhưng trước mặt gia đ&igrave;nh người y&ecirc;u, c&ocirc; kh&ocirc;ng d&aacute;m n&oacute;i g&igrave;. Qu&acirc;n th&igrave; ra sức b&ecirc;nh người y&ecirc;u, hết lời ca ngợi c&ocirc; t&agrave;i giỏi được l&agrave;m ở c&ocirc;ng ty nước ngo&agrave;i, ở nh&agrave; th&igrave; ngoan ngo&atilde;n với cha mẹ. Mẹ Qu&acirc;n nghe con n&oacute;i vậy th&igrave; cũng kh&ocirc;ng n&oacute;i th&ecirc;m g&igrave; nhưng Hoa biết, b&agrave; kh&oacute; c&oacute; thể chấp nhận c&ocirc;.</p>\r\n\r\n<p><a href="http://tieuthuyethay.com/wp-content/uploads/2015/11/sao-em-no-voi-lay-chong.jpg" style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; text-decoration: none; color: rgb(51, 51, 51);"><img alt="Sao em nỡ vội lấy chồng..." class="aligncenter size-full wp-image-212967" src="http://tieuthuyethay.com/wp-content/uploads/2015/11/sao-em-no-voi-lay-chong.jpg" style="border:0px; clear:both; display:block; font-family:inherit; font-size:inherit; font-stretch:inherit; font-style:inherit; font-variant:inherit; font-weight:inherit; height:auto; line-height:inherit; margin:0px auto 1.5em; max-width:90%; padding:0px; vertical-align:baseline; width:400px" /></a></p>\r\n\r\n<p>Khi đưa người y&ecirc;u về, Qu&acirc;n li&ecirc;n tục động vi&ecirc;n c&ocirc; &ldquo;Mẹ anh kh&ocirc;ng c&oacute; &yacute; g&igrave; đ&acirc;u, em đừng để &yacute;&rdquo;, &ldquo;người y&ecirc;u anh l&agrave; nhất&rdquo; khiến c&ocirc; đỡ buồn phần n&agrave;o. Hoa y&ecirc;u Qu&acirc;n đ&atilde; hơn một năm nhưng kh&ocirc;ng hề biết nh&agrave; anh kh&aacute; giả n&ecirc;n bị mẹ Qu&acirc;n ch&ecirc; bai &ldquo;đ&agrave;o mỏ&rdquo;, c&ocirc; cảm thấy bị tổn thương. &ldquo;M&igrave;nh sẽ cố gắng, nhất định m&igrave;nh sẽ l&agrave;m mẹ anh ấy chấp nhận&rdquo; &ndash; Hoa tự nhủ, c&ocirc; tin t&igrave;nh y&ecirc;u ch&acirc;n th&agrave;nh của m&igrave;nh sẽ chiến thắng mọi thứ.</p>\r\n\r\n<p>Mọi chuyện kh&ocirc;ng dễ d&agrave;ng như Hoa tưởng, d&ugrave; c&ocirc; năng đến nh&agrave; thăm mẹ Qu&acirc;n hơn, thi thoảng cũng tặng qu&agrave; cho b&agrave; nhưng mẹ Qu&acirc;n vẫn kh&ocirc;ng ưa c&ocirc;. Một ng&agrave;y, khi Qu&acirc;n kh&ocirc;ng ở nh&agrave;, b&agrave; đ&atilde; n&oacute;i thẳng với c&ocirc;: &ldquo;B&aacute;c nghĩ ch&aacute;u c&ograve;n trẻ, tương lai c&ograve;n nhiều hứa hẹn, chắc chắn ch&aacute;u sẽ gặp được người th&iacute;ch hợp với m&igrave;nh. Ch&aacute;u nh&igrave;n ch&aacute;u xem, mới c&oacute; hai mấy tuổi đầu, biết c&aacute;i g&igrave; m&agrave; chăm lo cho thằng Qu&acirc;n được. Chưa kể nh&agrave; ch&aacute;u kh&ocirc;ng c&oacute; nh&agrave; H&agrave; Nội, chắc cũng chẳng c&oacute; của nải g&igrave; để d&agrave;nh, lại l&agrave; con g&aacute;i ngoại tỉnh. Ch&aacute;u nghĩ m&igrave;nh với được Qu&acirc;n &agrave;? B&aacute;c ra kia vơ một c&aacute;i cũng được cả đống đứa như ch&aacute;u. B&aacute;c n&oacute;i ch&aacute;u biết, v&igrave; ch&aacute;u m&agrave; thằng Qu&acirc;n nh&agrave; n&agrave;y từ bỏ mối th&ocirc;ng gia với &ocirc;ng gi&aacute;m đốc ng&acirc;n h&agrave;ng n&oacute;, nếu n&oacute; lấy con g&aacute;i &ocirc;ng ấy, c&oacute; phải giờ đ&atilde; l&ecirc;n trưởng ph&ograve;ng rồi kh&ocirc;ng?&hellip;.&rdquo;.</p>\r\n\r\n<p>Hoa lặng người khi nghe mẹ Qu&acirc;n n&oacute;i, b&agrave; kh&ocirc;ng những x&uacute;c phạm c&ocirc; m&agrave; c&ograve;n động chạm cả đến bố mẹ c&ocirc;. Đến l&uacute;c n&agrave;y th&igrave; Hoa kh&ocirc;ng nhịn được nữa, c&ocirc; đ&acirc;u phải loại con g&aacute;i thấp k&eacute;m như b&aacute;c ấy n&oacute;i. Hoa gạt nhẹ những giọt nước mắt vừa tr&agrave;o ra, lạnh l&ugrave;ng ch&agrave;o mẹ Qu&acirc;n rồi quay bước đi thẳng.</p>\r\n\r\n<p>Hoa v&agrave; Qu&acirc;n đ&atilde; c&oacute; một trận c&atilde;i v&atilde; lớn sau đ&oacute;. Qu&acirc;n bảo Hoa h&atilde;y ki&ecirc;n nhẫn để anh thuyết phục mẹ c&ograve;n Hoa th&igrave; bắt anh chọn c&ocirc; hoặc mẹ anh. Giữa l&uacute;c Qu&acirc;n ph&acirc;n v&acirc;n chưa biết l&agrave;m sao cho hợp l&yacute; th&igrave; Hoa tuy&ecirc;n bố: &ldquo;Nếu anh kh&ocirc;ng chọn được, th&igrave; ng&agrave;y n&agrave;y th&aacute;ng sau em mời anh đi dự đ&aacute;m cưới của em. Anh nghĩ m&igrave;nh anh cao gi&aacute; chắc, em cũng nhiều người theo đuổi lắm đấy&hellip;&rdquo;. Qu&acirc;n tưởng người y&ecirc;u n&oacute;i đ&ugrave;a, anh kh&ocirc;ng để t&acirc;m lắm. H&agrave;ng ng&agrave;y, Qu&acirc;n vẫn t&igrave;m c&aacute;ch k&eacute;o hai người phụ nữ anh y&ecirc;u lại gần nhau hơn. Đ&ugrave;ng một c&aacute;i, vừa tr&ograve;n một th&aacute;ng sau lời tuy&ecirc;n bố định mệnh ấy, Hoa nhờ bạn gửi thiệp cưới cho anh.</p>\r\n\r\n<p>Qu&acirc;n kh&ocirc;ng thể tin v&agrave;o mắt m&igrave;nh, anh kh&ocirc;ng ngờ người y&ecirc;u m&igrave;nh c&oacute; thể h&agrave;nh động xốc nổi đến thế. Anh ch&agrave;ng ghi t&ecirc;n trong thiệp cưới kia Qu&acirc;n kh&ocirc;ng xa lạ g&igrave;, l&agrave; một cậu l&agrave;m c&ugrave;ng c&ocirc;ng ty với Hoa v&agrave; theo đuổi Hoa đ&atilde; l&acirc;u. Chắc hẳn Hoa đ&atilde; qu&aacute; giận anh n&ecirc;n mới l&agrave;m ra chuyện thiếu suy nghĩ thế n&agrave;y. Anh gọi cho Hoa kh&ocirc;ng được, đến C&ocirc;ng ty Hoa bảo vệ kh&ocirc;ng cho v&agrave;o. Anh ki&ecirc;n quyết chờ ở dưới c&ocirc;ng ty đến khi gặp được Hoa. Nhưng l&uacute;c gặp, Hoa chỉ nh&igrave;n anh như người xa lạ v&agrave; đuổi anh đi. Qu&acirc;n n&oacute;i cạn lời nhưng Hoa vẫn kh&ocirc;ng mảy may ch&uacute; &yacute; tới. L&uacute;c n&agrave;y, anh &ldquo;chồng sắp cưới&rdquo; của Hoa xuất hiện. Hai người đ&aacute;nh nhau to trước cổng c&ocirc;ng ty v&agrave; chỉ dừng lại khi bảo vệ to&agrave; nh&agrave; chạy ra can.</p>\r\n\r\n<p>Chỉ c&ograve;n một tuần nữa đ&aacute;m cưới diễn ra, ng&agrave;y n&agrave;o Qu&acirc;n cũng đứng trước cửa c&ocirc;ng ty đợi Hoa về để n&oacute;i chuyện. Nhưng mọi nỗ lực của Qu&acirc;n đều v&ocirc; &iacute;ch, Hoa vẫn l&ecirc;n xe Hoa đ&uacute;ng như kế hoạch. H&ocirc;m đấy bạn b&egrave; của Qu&acirc;n phải x&uacute;m lại giữ anh v&igrave; sợ anh x&ocirc;ng v&agrave;o&hellip;cướp c&ocirc; d&acirc;u. Nh&igrave;n Hoa l&ecirc;n xe về nh&agrave; chồng, Qu&acirc;n gục xuống kh&oacute;c như một đứa trẻ. Hoa cũng kh&ocirc;ng hơn g&igrave; anh &ldquo;Giờ đ&atilde; qu&aacute; muộn rồi Qu&acirc;n &agrave;, em xin lỗi&hellip;&rdquo; &ndash; Hoa khẽ n&oacute;i qua l&agrave;n nước mắt, nh&igrave;n anh qua gương chiếu hậu m&agrave; l&ograve;ng c&ocirc; đau quặn thắt.</p>\r\n\r\n<p>V&igrave; tổn thương, v&igrave; n&ocirc;ng nổi m&agrave; hai người họ đ&atilde; bỏ lỡ nhau. Biết tr&aacute;ch số phận hay tr&aacute;ch ch&iacute;nh bản th&acirc;n đ&atilde; kh&ocirc;ng cố bảo vệ hạnh ph&uacute;c của m&igrave;nh? Chắc chỉ hai người họ mới c&oacute; c&acirc;u trả lời cho ri&ecirc;ng m&igrave;nh.</p>\r\n\r\n<p>Theo Th&uacute;y Quỳnh / Tr&iacute; Thức Trẻ</p>\r\n', 'sao-em-no-voi-lay-chong.jpg', 1, 2, '2015-11-29 08:49:41.000000', NULL, 1, '28-Nov-2015'),
(11, 'Chàng luật sư và cô nhóc', '<p>Văn ph&ograve;ng luật sư</p>\r\n\r\n<p>T&ocirc;i chăm ch&uacute; nh&igrave;n anh.</p>\r\n\r\n<p>Anh cười ha hả &ldquo;Th&ocirc;i đi pha caf&eacute; đi&rdquo;. T&ocirc;i v&acirc;ng v&acirc;ng dạ dạ rồi đi pha caf&eacute; v&agrave; mang ra cho anh. T&ocirc;i thay nước cho b&igrave;nh hoa, dọn dẹp văn ph&ograve;ng, sắp xếp lại đống giấy tờ cho gọn g&agrave;ng. Chỉ mất một tiếng, t&ocirc;i ho&agrave;n th&agrave;nh c&ocirc;ng việc đ&atilde; đều đặn l&agrave;m trong suốt 2 năm. T&ocirc;i ngồi chống cằm mơ m&agrave;ng nh&igrave;n anh.. Chẳng biết điều g&igrave; tr&ecirc;n khu&ocirc;n mặt anh lại c&oacute; sức h&uacute;t mạnh mẽ với t&ocirc;i đến thế.. T&ocirc;i c&oacute; thể im lặng h&agrave;ng giờ để nh&igrave;n anh. V&agrave; anh th&igrave; kh&ocirc;ng bao giờ thắc mắc về h&agrave;nh động k&igrave; lạ đ&oacute; của t&ocirc;i hay tỏ ra kh&oacute; chịu. Thi thoảng, anh sẽ ngưng c&ocirc;ng việc đang l&agrave;m, nh&igrave;n t&ocirc;i một c&aacute;i rồi cười. L&uacute;c ấy, t&ocirc;i cũng sẽ hạnh ph&uacute;c m&agrave; cười t&iacute;t mắt lại.</p>\r\n\r\n<p>&ndash; H&ocirc;m nay kh&ocirc;ng c&oacute; việc g&igrave; để em l&agrave;m đ&acirc;u. Ngồi đọc s&aacute;ch rồi l&aacute;t xong việc anh dẫn đi ăn nh&eacute; .T&ocirc;i gật gật đầu, cầm quyển s&aacute;ch trong tay đọc đọc v&agrave;i trang rồi lăn ra ngủ ngon l&agrave;nh =.=</p>\r\n\r\n<p>***</p>\r\n\r\n<p>Tại qu&aacute;n ăn H&agrave;n Quốc</p>\r\n\r\n<p>&ndash; Oppa, t&ocirc;i n&oacute;i với anh bằng giọng ngọt ng&agrave;o nhất c&oacute; thể, cho em gọi m&oacute;n n&agrave;y nh&aacute;, m&oacute;n n&agrave;y,m&oacute;n n&agrave;y v&agrave; cả m&oacute;n n&agrave;y nữa nh&aacute; :&rdquo;&gt;</p>\r\n\r\n<p>&ndash; Ừ, em gọi th&ecirc;m nữa đi. Gọi những m&oacute;n m&agrave; em th&iacute;ch đấy.</p>\r\n\r\n<p>T&ocirc;i nheo mắt, giả vờ nh&igrave;n anh bằng &aacute;nh mắt đầy nghi ngờ:</p>\r\n\r\n<p>&ndash; Anh mới tr&uacute;ng sổ số &agrave;? Hay anh l&agrave;m g&igrave; c&oacute; lỗi với em đấy???Sao h&ocirc;m nay lại kh&ocirc;ng cằn nhằn &ldquo;Em ăn nhiều thế anh l&agrave;m g&igrave; c&oacute; tiền m&agrave; trả?&rdquo; ???</p>\r\n\r\n<p>&ndash; Ơ, c&aacute;i c&ocirc; n&agrave;y, tốt với c&ocirc;, chiều c&ocirc; thế m&agrave; c&ocirc; cũng thắc mắc. Thế th&ocirc;i kh&ocirc;ng ăn nữa nh&aacute;.</p>\r\n', 'breathe_by_hard_to_be_a_girl-d3c141q1.jpg', 1, 4, '2015-11-29 02:50:18.000000', '2015-11-29 02:52:22.000000', 1, '30-Nov-2015');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`) VALUES
(33, 8, 1),
(34, 8, 2),
(43, 10, 1),
(44, 10, 5),
(45, 10, 8),
(48, 11, 6),
(49, 11, 9);

-- --------------------------------------------------------

--
-- Table structure for table `post_viewable`
--

CREATE TABLE IF NOT EXISTS `post_viewable` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_viewable`
--

INSERT INTO `post_viewable` (`id`, `post_id`, `user_id`) VALUES
(17, 10, 3),
(18, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE IF NOT EXISTS `privacy` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`id`, `name`) VALUES
(1, 'Private'),
(2, 'Protected 1'),
(3, 'protected 2'),
(4, 'Public');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `owner_id` int(11) NOT NULL,
  `color` varchar(20) NOT NULL,
  `address` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `subject`, `content`, `from`, `to`, `owner_id`, `color`, `address`, `created_at`) VALUES
(5, 'Ăn sáng', 'Bánh mì pa-te', '2015-11-17 06:25:17', '2015-11-17 06:55:10', 1, '#00ff00', 'Cổng Trần Đại Nghĩa', '2015-11-17 09:38:31'),
(7, 'Đi chơi với bạn gái', 'Xem phim ', '2015-11-19 17:25:36', '2015-11-18 22:50:36', 1, '#cc0000', 'Rạp chiếu phim Quốc Gia', '2015-11-17 10:33:41'),
(8, 'Code web', 'Code blog', '2015-11-19 21:25:41', '2015-11-04 01:25:41', 1, '#274e13', 'Thư viện', '2015-11-19 09:32:32'),
(9, 'hehehe', 'hehehe', '2015-11-19 10:05:43', '2015-11-18 10:00:43', 3, '#00ff00', 'acnjkcnkj', '2015-11-25 01:03:30'),
(10, 'Làm báo cáo', 'Hoàn thành báo cáo về yêu cầu + sơ đồ E-R', '2015-12-02 07:00:27', '2015-12-03 14:45:27', 1, '#ff0000', 'Ở nhà', '2015-12-01 11:44:21');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_user`
--

CREATE TABLE IF NOT EXISTS `schedule_user` (
  `id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule_user`
--

INSERT INTO `schedule_user` (`id`, `schedule_id`, `user_id`) VALUES
(8, 10, 3),
(9, 10, 2),
(10, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'Truyện dài'),
(2, 'Nhật ký chia tay'),
(3, 'name'),
(4, 'acnsljsn'),
(5, 'Nhật ký tình yêu'),
(6, 'Truyện ngắn'),
(7, 'Truyện tình yêu'),
(8, 'Sao em lỡ vội lấy chồng'),
(9, 'Luật sư');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `hobby` text,
  `address` text,
  `work` text,
  `phone_number` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level_id` smallint(4) NOT NULL,
  `desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `first_name`, `last_name`, `hobby`, `address`, `work`, `phone_number`, `status`, `created_at`, `updated_at`, `level_id`, `desc`, `image`, `auth_key`, `password_reset_token`) VALUES
(1, 'dalv', 'dalv0911@gmail.com', '$2y$13$ZA0vSEc1AfyD9q.qGyGJ4OL1jdlWn5ijIU9TUo2WHo/6MJoI8xQES', 'Le', 'Da', 'Nope', 'Vô gia cư', 'Sinh viên', '+841688279742', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'Tôi xin phép được giữ im lặng', '10393905_1586166241635188_5766472032032390248_n.jpg', 'OeAtuinLn5o-6kooe7RUAG-TwZiwLFHz', NULL),
(2, 'tuanna', 'tuanna@gmail.com', '$2y$13$ZR1zHI5qPs/BaT2cSGqjtenfB1niOfSICSjJUCIaoSNOgva2pdnAC', 'Anh', 'Tuấn', 'Chém gió', 'Vô gia cư', 'Sinh viên', '+84123456789', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu ca hát, thích màu hồng, ghét sự giả dối.', '4a62a0341870c7adf63ed757e208649f7fa7a2a8.jpg', 'xGtJ6Qug14aUz-ndJ1bw8hncjbX8u2aX', NULL),
(3, 'chungtv', 'chungtv@gmail.com', '$2y$13$GwFOb6r3I8BGk7VJ4Qm41el0id0dA3.k4T4ZqWDSMhtctS4yaXK2q', 'Trần', 'Chung', 'Ngắm trai', 'Vô gia cư', 'Dev', '+841688279742', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu tím', 'DSC_9405.jpg', 'ymQLeidwDyM7OkrFDGE9wPro9hYjhhy8', NULL),
(4, 'kient', 'kient@gmail.com', '$2y$13$G2NgMyNOr4pES08hkhXeoOk.9azsvutAWx3HOMsSRn6ISPW/Llf1m', 'Trịnh', 'Kiên', 'Tán gái', 'Vô gia cư', 'Tán gái kiếm tiền', '1234567891', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu hồng', '', '3WUta84-WNkHxVmLS0kGNqMfzXtMThJm', NULL),
(5, 'vunh', 'vunh@gmail.com', '$2y$13$uUM3J9rC/0DsrLRwBSRtU.qbIj0WAaS6uojCCsiPFnNN2abVf8ZwW', 'Hoàng', 'Vũ', 'Tán gái', 'Ngõ tự do', 'Tán gái kiếm tiền', '1234567890', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Yêu màu tím, ghét sự giả dối', 'DSC_9405.jpg', 'EZwB2327-qcv6pE0XlJIwTHeyTowrLCv', NULL),
(6, 'thanhph', 'thanhph@gmail.com', '$2y$13$nY3w/M9YFDukrjPFBIN6sOVaxC4Tvty5SDdUkJtiHuZtX90Ce1xLS', 'Thanh', 'Phạm', 'Tán gái', 'Cầu thanh trì', 'Sinh viên', '123456782345', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'Nope hehe', 'IMG_20151028_162239.jpg', 'j9PANZOfk2ZqTxSM59u1MkFL6353ZMhB', NULL),
(7, 'tungnt', 'tungnt@gmail.com', '$2y$13$33IHuIDgjhw2JKETmjeh7umEF7H1rdodZVXa8T0znUKcuku9/1fqK', 'Thanh', 'Tung', 'ancscslckn', 'anccjnjcnk', 'cnaclnslkcnclk', '198208049824', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'ncacnasklcnl', 'DSC_9377.jpg', 'vuAJaoIxmUzvZ97CN76K1u7fWB3mblQt', NULL),
(8, 'haipv', 'haipv@gmail.com', '$2y$13$EVHutu6vk9wh3x8.Xtk5weNx9fEzId1XmpmPnAz0gjOV3RCDkTmzK', 'Phạm', 'Hải', '', '', '', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '', 'Jy_OTpoiLYrQD6HGdPCNg8DLLVK0SXNM', NULL),
(9, 'maipl', 'maipl@gmail.com', '$2y$13$3WwqzxcDugsjF.4/GqlDN.PcfrWcaRVBWyrpxNnGPzWjIifHm2MoW', 'Lan', 'Mai', '', '', '', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '14892809.jpg', 'LgFM8-yqSFd-ACwvssL-5O8x8gCYR-P1', NULL),
(10, 'minhnh', 'minhnh@gmail.com', '$2y$13$tGhxJuGqV54IuIh5hxhwNeupIbs80cHh5oOSzKe6.xxK4l.VhD.P.', 'Hoàng', 'Minh', '', '', '', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '12295424_1766736786917139_3120918293341319787_n.jpg', 'SEF_rdC_r_taT-mAwwEq-IdSlEQC7uBz', NULL),
(11, 'tunnm', 'tunnm@gmail.com', '$2y$13$aTwRquESr.a7A6ByDw96LeZ9qe7QD.xLaaFiUijomSLUwDZ6E1rai', 'Minh', 'Tú', '', '', '', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 'breathe_by_hard_to_be_a_girl-d3c141q1.jpg', '1gIirUsc4o6E0oJMHfdPpRFX-SnefJxT', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_comment` (`post_id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_friend`
--
ALTER TABLE `group_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_user_message` (`message_group_id`),
  ADD KEY `group_user_user` (`user_id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_message_group` (`received_group_id`),
  ADD KEY `message_user` (`send_user_id`);

--
-- Indexes for table `message_group`
--
ALTER TABLE `message_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_user` (`from_user_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_post` (`owner_id`),
  ADD KEY `privacy_post` (`privacy_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_post_tag` (`tag_id`),
  ADD KEY `post_post_tag` (`post_id`);

--
-- Indexes for table `post_viewable`
--
ALTER TABLE `post_viewable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_viewable_post` (`post_id`),
  ADD KEY `post_viewable_user` (`user_id`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_schedule` (`owner_id`),
  ADD KEY `privacy_schedule` (`color`);

--
-- Indexes for table `schedule_user`
--
ALTER TABLE `schedule_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedule_user_schedule` (`schedule_id`),
  ADD KEY `schedule_user_user` (`user_id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `group_friend`
--
ALTER TABLE `group_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `message_group`
--
ALTER TABLE `message_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `post_viewable`
--
ALTER TABLE `post_viewable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `schedule_user`
--
ALTER TABLE `schedule_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `post_comment` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`);

--
-- Constraints for table `group_user`
--
ALTER TABLE `group_user`
  ADD CONSTRAINT `group_user_message` FOREIGN KEY (`message_group_id`) REFERENCES `message_group` (`id`),
  ADD CONSTRAINT `group_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_message_group` FOREIGN KEY (`received_group_id`) REFERENCES `message_group` (`id`),
  ADD CONSTRAINT `message_user` FOREIGN KEY (`send_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_user` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `privacy_post` FOREIGN KEY (`privacy_id`) REFERENCES `privacy` (`id`),
  ADD CONSTRAINT `user_post` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_post_tag` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `tag_post_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

--
-- Constraints for table `post_viewable`
--
ALTER TABLE `post_viewable`
  ADD CONSTRAINT `post_viewable_post` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `post_viewable_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `user_schedule` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `schedule_user`
--
ALTER TABLE `schedule_user`
  ADD CONSTRAINT `schedule_user_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`),
  ADD CONSTRAINT `schedule_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
